/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.expert.core;

import CLIPSJNI.*;
import java.util.ArrayList;
/**
 *
 * @author Ahmad
 */
public class ClipsHelper {
    
    private static Environment clips = null;
    
    FrameGenerator frameGenerator;

    public ClipsHelper() {
        // Frame Generator
        frameGenerator = new FrameGenerator();
        frameGenerator.generateFrameMap();
    }
    
    
    public static Environment getClips() {
        if(clips == null) {
            clips = new Environment();
        }
        
        return clips;
    }
    
    public static Environment GenerateCLIPSFrame() {
        Environment clips = new Environment();
        clips.load("frame/class_recommendation.clp");
        clips.load("frame/class_recommendations.clp");
        clips.load("frame/class_sensor.clp");
        clips.load("frame/class_sensor_group.clp");
        clips.load("frame/class_sensor_var.clp");
        clips.load("frame/class_sensor_rec.clp");
        
        return clips;
    }

    public FrameGenerator getFrameGenerator() {
        return frameGenerator;
    }
    
    public Environment GenerateCLIPSFrame(String frameName) {
        Environment clips = new Environment();
        clips.load("frame/class_recommendation.clp");
        clips.load("frame/class_recommendations.clp");
        clips.load("frame/class_sensor.clp");
        clips.load("frame/class_sensor_group.clp");
        clips.load("frame/class_sensor_var.clp");
        clips.load("frame/class_sensor_rec.clp");
        
        ArrayList<String> frameList = frameGenerator.getFrameMap().get(frameName);
        for(String frame : frameList) {
            clips.eval(frame);
        }

        return clips;
    }
}
