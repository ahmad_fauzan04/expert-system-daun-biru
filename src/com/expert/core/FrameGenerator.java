/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.expert.core;

import com.expert.database.Service;
import com.expert.util.FileUtil;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ahmad
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class FrameGenerator {
    Hashtable<String, ArrayList<String>> frameMap;
    Hashtable<String, Integer> indexMap;
    
    public FrameGenerator() {
        frameMap = new Hashtable<>();
        indexMap = new Hashtable<>();
    }
    
    
    
    @SuppressWarnings("ConvertToStringSwitch")
    public void generateFrameFiles() {
        String folder = "gen/";
        File folderFile = new File(folder);
        if(!folderFile.exists()) {
            folderFile.mkdir();
        }
        ArrayList<Hashtable<String,String>> sensor_groups = Service.getSensorGroups();
        for(Hashtable<String,String> sensor_group : sensor_groups) {
            String stream = ";; Generated CLP \n";
            String sensor_group_string = ";; Sensor Group\n"+
                "(make-instance [sensor_group.name] of SensorGroup\n" +
                "\t(Name [sensor_group.name])\n" +
                "\t(HighLimit [sensor_group.high_limit])\n" +
                "\t(LowLimit [sensor_group.low_limit])\n" +
                ")\n";
            sensor_group_string = sensor_group_string.replaceAll("\\[sensor_group.name\\]", 
                    sensor_group.get("name"));
            sensor_group_string = sensor_group_string.replaceAll("\\[sensor_group.high_limit\\]", 
                    sensor_group.get("high_limit"));
            sensor_group_string = sensor_group_string.replaceAll("\\[sensor_group.low_limit\\]", 
                    sensor_group.get("low_limit"));
            
            stream += sensor_group_string;
            
            // Get Sensor
            stream += ";; Sensor";
            
            String sensor_string_template = "\n(make-instance [sensor.name] of Sensor\n" +
                    "	(Name [sensor.name])\n" +
                    "	(Value 0)\n" +
                    ")\n" +
                    "(send [sensor_group.name] add-Sensor [sensor.name])\n";
            
            sensor_string_template = sensor_string_template.replaceAll("\\[sensor_group.name\\]", 
                    sensor_group.get("name"));
            
            ArrayList<Hashtable<String,String>> sensors = 
                    Service.getSensor(Integer.parseInt(sensor_group.get("id_sensor_group")));
            for(Hashtable<String,String> sensor : sensors) {
                String sensor_string = sensor_string_template;
                sensor_string = sensor_string.replaceAll("\\[sensor.name\\]", 
                        sensor.get("name"));
                
                stream += sensor_string;
            }
            
            // Get Sensor Var
            
            stream += "\n;; SensorVar";
            
            String sensorvar_string_template = "\n(make-instance [sensor_var.name] of SensorVar\n" +
                            "	(Question [sensor_var.question])\n" +
                            ")\n" +
                            "\n" +
                            "(send [sensor_group.name] [add-if] [sensor_var.name])\n";
            
            sensorvar_string_template = sensorvar_string_template.replaceAll("\\[sensor_group.name\\]", 
                    sensor_group.get("name"));
            
            ArrayList<Hashtable<String,String>> sensorvars = 
                    Service.getSensorVar(Integer.parseInt(sensor_group.get("id_sensor_group")));
            for(Hashtable<String,String> sensorvar : sensorvars) {
                String sensorvar_string = sensorvar_string_template;
                
                sensorvar_string = sensorvar_string.replaceAll("\\[sensor_var.name\\]", 
                        sensor_group.get("name")+"_"+sensorvar.get("state")
                                +"_"+sensorvar.get("id_sensor_var"));
                sensorvar_string = sensorvar_string.replaceAll("\\[sensor_var.question\\]", 
                        "\"" + sensorvar.get("question") + "\"");
                sensorvar_string = sensorvar_string.replaceAll("\\[sensor_group.name\\]", 
                        sensor_group.get("name"));
                
                if(sensorvar.get("state").equals("group_high")) {
                    sensorvar_string = sensorvar_string.replaceAll("\\[add-if\\]", 
                        "add-IfGroupHigh");
                } else if(sensorvar.get("state").equals("group_low")) {
                    sensorvar_string = sensorvar_string.replaceAll("\\[add-if\\]", 
                        "add-IfGroupLow");
                } else if(sensorvar.get("state").equals("sensor_high")) {
                    sensorvar_string = sensorvar_string.replaceAll("\\[add-if\\]", 
                        "add-IfSensorHigh");
                } else { // sensor_low
                    sensorvar_string = sensorvar_string.replaceAll("\\[add-if\\]", 
                        "add-IfSensorLow");
                }
                
                stream += sensorvar_string;
                
                // Recommendation
                String recommendation_string_template = "\n(make-instance [recomendation.name] of SensorRec\n" +
                    "	(Value [recomendation.value])\n" +
                    ")\n" +
                    "(send [sensor_var.name] add-SensorRec [recommendation.name])\n";
                
                recommendation_string_template = recommendation_string_template.replaceAll(
                        "\\[sensor_var.name\\]", 
                        sensor_group.get("name")+"_"+sensorvar.get("state")
                                +"_"+sensorvar.get("id_sensor_var"));
                
                ArrayList<Hashtable<String,String>> recommendations = 
                        Service.getRecommendation(Integer.parseInt(sensorvar.get("id_sensor_var")));
                
                for(Hashtable<String,String> recommendation : recommendations) {
                    String recommendation_string = recommendation_string_template;
                    
                    recommendation_string = recommendation_string.replaceAll("\\[recomendation.name\\]", 
                            "recom_" + recommendation.get("id_sensor_recommendation"));
                    
                    recommendation_string = recommendation_string.replaceAll("\\[recomendation.value\\]", 
                            "\"" + recommendation.get("recommendation") + "\"");
                    
                    stream += recommendation_string;
                }
            }
            
            stream += "\n(make-instance Recom of Recommendations)";
            
            try {
                FileUtil.Write(stream,
                        folder+"frame_" + sensor_group.get("name") +".clp");
            } catch (IOException ex) {
                Logger.getLogger(FrameGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    public int getIndex(String frameName) {
        return indexMap.get(frameName);
    }
    
    @SuppressWarnings("ConvertToStringSwitch")
    public void generateFrameMap() {
        indexMap.clear();
        frameMap.clear();
        ArrayList<Hashtable<String,String>> sensor_groups = Service.getSensorGroups();
        for(Hashtable<String,String> sensor_group : sensor_groups) {
            
            // Add to index map
            indexMap.put(sensor_group.get("name"), Integer.parseInt(sensor_group.get("id_sensor_group")));
            
            // ArrayList
            ArrayList<String> frameList = new ArrayList<>();
            
            String sensor_group_string = ";; Sensor Group\n"+
                "(make-instance [sensor_group.name] of SensorGroup\n" +
                "\t(Name [sensor_group.name])\n" +
                "\t(HighLimit [sensor_group.high_limit])\n" +
                "\t(LowLimit [sensor_group.low_limit])\n" +
                ")\n";
            sensor_group_string = sensor_group_string.replaceAll("\\[sensor_group.name\\]", 
                    sensor_group.get("name"));
            sensor_group_string = sensor_group_string.replaceAll("\\[sensor_group.high_limit\\]", 
                    sensor_group.get("high_limit"));
            sensor_group_string = sensor_group_string.replaceAll("\\[sensor_group.low_limit\\]", 
                    sensor_group.get("low_limit"));
            
            frameList.add(sensor_group_string);
            
            String sensor_string_template = "(make-instance [sensor.name] of Sensor\n" +
                    "	(Name [sensor.name])\n" +
                    "	(Value 0)\n" +
                    ")";
                    
            String sensor_string_template_send = "(send [sensor_group.name] add-Sensor [sensor.name])\n";
            
            sensor_string_template = sensor_string_template.replaceAll("\\[sensor_group.name\\]", 
                    sensor_group.get("name"));
            
            sensor_string_template_send = sensor_string_template_send.replaceAll("sensor_group.name", 
                    sensor_group.get("name"));
            
            ArrayList<Hashtable<String,String>> sensors = 
                    Service.getSensor(Integer.parseInt(sensor_group.get("id_sensor_group")));
            for(Hashtable<String,String> sensor : sensors) {
                String sensor_string = sensor_string_template;
                String sensor_string_send = sensor_string_template_send;
                sensor_string = sensor_string.replaceAll("\\[sensor.name\\]", 
                        sensor.get("name"));
                sensor_string_send = sensor_string_send.replaceAll("sensor.name", 
                        sensor.get("name"));
                
                frameList.add(sensor_string);
                frameList.add(sensor_string_send);
            }
            
            // Get Sensor Var
            
            String sensorvar_string_template = "(make-instance [sensor_var.name] of SensorVar\n" +
                            "	(Question [sensor_var.question])\n" +
                            ")\n" +
                            "\n";
                            
            String sensorvar_string_template_send = "(send [sensor_group.name] [add-if] [sensor_var.name])\n";
            
            sensorvar_string_template = sensorvar_string_template.replaceAll("\\[sensor_group.name\\]", 
                    sensor_group.get("name"));
            
            sensorvar_string_template_send = sensorvar_string_template_send.replaceAll("sensor_group.name", 
                    sensor_group.get("name"));
            
            ArrayList<Hashtable<String,String>> sensorvars = 
                    Service.getSensorVar(Integer.parseInt(sensor_group.get("id_sensor_group")));
            for(Hashtable<String,String> sensorvar : sensorvars) {
                String sensorvar_string = sensorvar_string_template;
                String sensorvar_string_send = sensorvar_string_template_send;
                
                sensorvar_string = sensorvar_string.replaceAll("\\[sensor_var.name\\]", 
                        sensor_group.get("name")+"_"+sensorvar.get("state")
                                +"_"+sensorvar.get("id_sensor_var"));
                sensorvar_string = sensorvar_string.replaceAll("\\[sensor_var.question\\]", 
                        "\"" + sensorvar.get("question") + "\"");
                sensorvar_string = sensorvar_string.replaceAll("\\[sensor_group.name\\]", 
                        sensor_group.get("name"));
                
                sensorvar_string_send = sensorvar_string_send.replaceAll("sensor_var.name", 
                        sensor_group.get("name")+"_"+sensorvar.get("state")
                                +"_"+sensorvar.get("id_sensor_var"));
                
                if(sensorvar.get("state").equals("group_high")) {
                    sensorvar_string_send = sensorvar_string_send.replaceAll("\\[add-if\\]", 
                        "add-IfGroupHigh");
                } else if(sensorvar.get("state").equals("group_low")) {
                    sensorvar_string_send = sensorvar_string_send.replaceAll("\\[add-if\\]", 
                        "add-IfGroupLow");
                } else if(sensorvar.get("state").equals("sensor_high")) {
                    sensorvar_string_send = sensorvar_string_send.replaceAll("\\[add-if\\]", 
                        "add-IfSensorHigh");
                } else { // sensor_low
                    sensorvar_string_send = sensorvar_string_send.replaceAll("\\[add-if\\]", 
                        "add-IfSensorLow");
                }
                
                frameList.add(sensorvar_string);
                frameList.add(sensorvar_string_send);
                
                // Recommendation
                String recommendation_string_template = "(make-instance [recomendation.name] of SensorRec\n" +
                    "	(Value [recomendation.value])\n" +
                    ")\n" ;
                    
                String recommendation_string_template_send = "(send [sensor_var.name] add-SensorRec [recomendation.name])\n";
                
                recommendation_string_template = recommendation_string_template.replaceAll(
                        "\\[sensor_var.name\\]", 
                        sensor_group.get("name")+"_"+sensorvar.get("state")
                                +"_"+sensorvar.get("id_sensor_var"));
                
                recommendation_string_template_send = recommendation_string_template_send.replaceAll(
                        "sensor_var.name", 
                        sensor_group.get("name")+"_"+sensorvar.get("state")
                                +"_"+sensorvar.get("id_sensor_var"));
                
                ArrayList<Hashtable<String,String>> recommendations = 
                        Service.getRecommendation(Integer.parseInt(sensorvar.get("id_sensor_var")));
                
                for(Hashtable<String,String> recommendation : recommendations) {
                    String recommendation_string = recommendation_string_template;
                    String recommendation_string_send = recommendation_string_template_send;
                    
                    recommendation_string = recommendation_string.replaceAll("\\[recomendation.name\\]", 
                            "recom_" + recommendation.get("id_sensor_recommendation"));
                    
                    recommendation_string_send = recommendation_string_send.replaceAll("recomendation.name", 
                            "recom_" + recommendation.get("id_sensor_recommendation"));
                    
                    recommendation_string = recommendation_string.replaceAll("\\[recomendation.value\\]", 
                            "\"" + recommendation.get("recommendation") + "\"");
                    
                    frameList.add(recommendation_string);
                    System.out.println("Recommendation Name:  " + recommendation_string_send);
                    frameList.add(recommendation_string_send);
                            
                }
            }
            
            
            frameList.add("(make-instance Recom of Recommendations)");
            
            frameMap.put(sensor_group.get("name"), frameList);
        }
    }

    public Hashtable<String, ArrayList<String>> getFrameMap() {
        return frameMap;
    }
    
    public String[] getSonsorGroups() {
        return frameMap.keySet().toArray(new String[frameMap.size()]);
    }
    
    public static void main(String[] args) {
        new FrameGenerator().generateFrameMap();
    }
}
