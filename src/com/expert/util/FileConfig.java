/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.expert.util;

import java.io.IOException;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ahmad
 */
public class FileConfig {
    Hashtable<String, String> configValue;
    
    public FileConfig() {
        configValue = new Hashtable<>();
    }
    
    public void loadConfig(String fileConfig) {
        try {
            String[]  lines  = FileUtil.ReadLines(fileConfig);
            for (String line : lines) {
                String[] pair = line.split(":");
                if(pair.length == 2) {
                    configValue.put(pair[0], pair[1]);
                } else if(pair.length == 1) {
                    configValue.put(pair[0], "");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FileConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Hashtable<String, String> getConfigValue() {
        return configValue;
    }
    
}
