/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.expert.util;

import com.expert.core.FrameGenerator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ahmad
 */
public class URLUtil {
    public static String getResponseFromURL(String urlString) {
        
        String response = null;
         try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream responseStream = connection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(responseStream, "UTF-8"));
            String line = in.readLine();
            response = line;
            while(line != null) {
                line = in.readLine();
                if(line == null) break;
                response += "\n" + line;
            }
            in.close();
            System.out.println(response);
        } catch (MalformedURLException ex) {
            Logger.getLogger(FrameGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FrameGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return response;
    }
}
