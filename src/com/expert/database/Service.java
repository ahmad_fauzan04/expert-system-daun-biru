/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.expert.database;

import com.expert.core.FrameGenerator;
import com.expert.util.URLUtil;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Ahmad
 */
public class Service {
    
    public static ArrayList<Hashtable<String,String>> getRecommendation(int id_sensor_var) {
        //return getFromDatabase("SELECT * FROM sensor_recommendation WHERE id_sensor_var="
        //        + id_sensor_var);
        
        ArrayList<Hashtable<String,String>> result = new ArrayList<>();
       
        String response = URLUtil.getResponseFromURL("http://monita.ap01.aws.af.cm/services/recommendation.php?id="+id_sensor_var);
         
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONObject resultObjects = jsonResponse.getJSONObject("result");
            for(int i=0; i < resultObjects.getInt("length"); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(""+i);
                Iterator keys = resultObject.keys();
                Hashtable<String,String> resHash = new Hashtable<>();
                while(keys.hasNext()) {
                    String key = keys.next().toString();
                    resHash.put(key, resultObject.getString(key));
                }
                result.add(resHash);
            }
        } catch (JSONException ex) {
            Logger.getLogger(FrameGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return result;
    }
    
    public static ArrayList<Hashtable<String,String>> getSensorVar(int id_sensor_group) {
        //return getFromDatabase("SELECT id_sensor_var,state,question,value FROM sensor_var WHERE id_sensor_group="
        //    + id_sensor_group);
        
        ArrayList<Hashtable<String,String>> result = new ArrayList<>();
       
        String response = URLUtil.getResponseFromURL("http://monita.ap01.aws.af.cm/services/sensorvar.php?id="+id_sensor_group);
         
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONObject resultObjects = jsonResponse.getJSONObject("result");
            for(int i=0; i < resultObjects.getInt("length"); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(""+i);
                Iterator keys = resultObject.keys();
                Hashtable<String,String> resHash = new Hashtable<>();
                while(keys.hasNext()) {
                    String key = keys.next().toString();
                    resHash.put(key, resultObject.getString(key));
                }
                result.add(resHash);
            }
        } catch (JSONException ex) {
            Logger.getLogger(FrameGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return result;
    }
    
    public static ArrayList<Hashtable<String,String>> getSensor(int sensor_group_id) {
        //return getFromDatabase("SELECT * FROM sensor WHERE id_sensor_group="
        //        + sensor_group_id);
        
        ArrayList<Hashtable<String,String>> result = new ArrayList<>();
       
        String response = URLUtil.
                getResponseFromURL("http://monita.ap01.aws.af.cm/services/sensor.php?id="+sensor_group_id);
         
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONObject resultObjects = jsonResponse.getJSONObject("result");
            for(int i=0; i < resultObjects.getInt("length"); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(""+i);
                Iterator keys = resultObject.keys();
                Hashtable<String,String> resHash = new Hashtable<>();
                while(keys.hasNext()) {
                    String key = keys.next().toString();
                    resHash.put(key, resultObject.getString(key));
                }
                result.add(resHash);
            }
        } catch (JSONException ex) {
            Logger.getLogger(FrameGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return result;
    }
    
    public static ArrayList<Hashtable<String,String>> getSensorGroups() {
        //return getFromDatabase("SELECT * FROM sensor_group");
         ArrayList<Hashtable<String,String>> result = new ArrayList<>();
       
         String response = URLUtil.
                 getResponseFromURL("http://monita.ap01.aws.af.cm/services/sensorgroup.php");
         
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONObject resultObjects = jsonResponse.getJSONObject("result");
            for(int i=0; i < resultObjects.getInt("length"); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(""+i);
                Iterator keys = resultObject.keys();
                Hashtable<String,String> resHash = new Hashtable<>();
                while(keys.hasNext()) {
                    String key = keys.next().toString();
                    resHash.put(key, resultObject.getString(key));
                }
                result.add(resHash);
            }
        } catch (JSONException ex) {
            Logger.getLogger(FrameGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         
         
         return result;
    }

    public static String getData(int sensor_group_id) {
        return URLUtil.getResponseFromURL("http://monita.ap01.aws.af.cm/services/execute.php?id="+sensor_group_id);
    }
}
