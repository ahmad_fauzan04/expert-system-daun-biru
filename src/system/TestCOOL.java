/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system;

import CLIPSJNI.Environment;
import CLIPSJNI.PrimitiveValue;
import com.expert.core.ClipsHelper;
import com.expert.core.FrameGenerator;
import java.util.ArrayList;

/**
 *
 * @author Ahmad
 */
public class TestCOOL {
    
    public static void main(String[] args) throws Exception {
        Environment clips = ClipsHelper.GenerateCLIPSFrame();
        
        FrameGenerator generator = new FrameGenerator();
        generator.generateFrameMap();
        String[] sensor_groups = generator.getSonsorGroups();
        
        for(int i=0; i < sensor_groups.length; i++) {
            ArrayList<String> frameList = generator.getFrameMap().get(sensor_groups[i]);
            for(String frame : frameList) {
                System.out.println(frame);
                clips.eval(frame);
            }
        }
        
        clips.eval("(send [a] set-Value 500)");
        
        PrimitiveValue sensors = clips.eval("(find-all-instances ((?f SensorVar)) (eq ?f:flag 1))");
        
        for(int i=0; i < sensors.size(); i++) {
            PrimitiveValue sensor = sensors.get(i);
            System.out.println(sensor.instanceNameValue());
            PrimitiveValue pv = clips.eval("(send ["+sensor.instanceNameValue()+"] get-Question)");
            System.out.println(pv.toString());
            pv = clips.eval("(send ["+sensor.instanceNameValue()+"] get-flag)");
            System.out.println(pv.toString());
        }
        
    }
}
