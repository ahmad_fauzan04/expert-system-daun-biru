README

REQUIREMENT
1. Java 1.6+
2. Internet Connection

RUN
1. Run / double-click "Daun_Biru_Expert.bat"
2. Click "Connect" Button and let the application detect the problem.
3. When the problem appears, select the problem, click "Start Inference" button.
4. When the question appears, answer the question.
5. After that, you'll see the Recommendation in the "Recommendation" field.