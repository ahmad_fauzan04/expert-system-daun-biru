;; Generated CLP 
;; Sensor Group
(make-instance exhaust of SensorGroup
	(Name exhaust)
	(HighLimit 490)
	(LowLimit 200)
)
;; Sensor
(make-instance a of Sensor
	(Name a)
	(Value 0)
)
(send exhaust add-Sensor a)

(make-instance b of Sensor
	(Name b)
	(Value 0)
)
(send exhaust add-Sensor b)

(make-instance c of Sensor
	(Name c)
	(Value 0)
)
(send exhaust add-Sensor c)

(make-instance d of Sensor
	(Name d)
	(Value 0)
)
(send exhaust add-Sensor d)

(make-instance e of Sensor
	(Name e)
	(Value 0)
)
(send exhaust add-Sensor e)

(make-instance f of Sensor
	(Name f)
	(Value 0)
)
(send exhaust add-Sensor f)

(make-instance g of Sensor
	(Name g)
	(Value 0)
)
(send exhaust add-Sensor g)

(make-instance h of Sensor
	(Name h)
	(Value 0)
)
(send exhaust add-Sensor h)

(make-instance i of Sensor
	(Name i)
	(Value 0)
)
(send exhaust add-Sensor i)

;; SensorVar
(make-instance exhaust_group_high_1 of SensorVar
	(Question "Saluran gas buang bocor?")
)

(send exhaust add-IfGroupHigh exhaust_group_high_1)

(make-instance recom_1 of SensorRec
	(Value "Periksa sistem saluran gas buang yang bocor & perbaiki jika perlu")
)
(send exhaust_group_high_1 add-SensorRec [recommendation.name])

(make-instance exhaust_group_high_2 of SensorVar
	(Question "Saluran gas buang atau udara masuk tersumbat?")
)

(send exhaust add-IfGroupHigh exhaust_group_high_2)

(make-instance recom_2 of SensorRec
	(Value "Periksa saluran gas buang atau udara masuk. Bersihkan jika perlu")
)
(send exhaust_group_high_2 add-SensorRec [recommendation.name])

(make-instance exhaust_sensor_high_3 of SensorVar
	(Question "Setelan timing katup kurang sesuai?")
)

(send exhaust add-IfSensorHigh exhaust_sensor_high_3)

(make-instance recom_3 of SensorRec
	(Value "Periksa, ganti jika perlu, indikator (sensor)")
)
(send exhaust_sensor_high_3 add-SensorRec [recommendation.name])

(make-instance Recom of Recommendations)