<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Secure{
		var $CI;
		
		function is_ignored($first, $second){
			$ignore_list = array(
				array("account", "login"),
				array("account", "logout")
			);

			$found = FALSE;
			foreach ($ignore_list as $v){
				if (($first == $v[0]) && ($second == $v[1])){
					$found = TRUE;
					break;
				}
			}
			log_message("debug", "#IPJ : Controller {$first} - {$second} is ".($found ? "ignored" : "not ignored"));
			return $found;
		}

		/**
		 *  bypass proses jika HTTP request ditujukan ke controller unsecure
		 *  [PROSES] mengecek user yg me-request sudah terotentikasi atau belum
		 *  jika belum, meng-otentikasi user dari HTTP request yg diterima
		 */
		function is_authenticate(){
			$this->CI = &get_instance();
			$first = $this->CI->uri->segment(1);
			$second = $this->CI->uri->segment(2);
			
			$uri_string = $this->CI->uri->uri_string();
			$this->CI->load->helper("action");
			$page = uri_to_action($uri_string); //echo "page : $page<br>";

			log_message('debug', "#IPJ : Request to $uri_string");
			log_message("debug", "#IPJ : page = {$page}");

			$this->CI->load->model("m_menu");
			if (!empty($page)){
				if (!$this->is_ignored($first, $second) && $this->CI->m_menu->is_secure($page)){
					if (!($role_id = $this->CI->authentication->get_role_id())){
						$this->CI->session->set_flashdata("refer", $uri_string);
						redirect("/account/login/?continue=".site_url($uri_string));
					} else {
						if (!$this->CI->m_menu->has_access($page, $role_id)){
							log_message("debug", $this->CI->session->userdata("member_id")." has no access to {$page}");
							//$this->CI->session->set_flashdata("refer", $page);
							//redirect("/no_access");
							show_404();
						}
					}
				}
			}
		}
	}
?>