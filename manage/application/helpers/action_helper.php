<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('uri_to_action'))
{
	/**
	 * @param $uri /file[/method[/params]]
	 * @return /file[/method]
	 */ 
	function uri_to_action($uri){
		if (!$uri){
			return null;
		}
		preg_match_all("/([^\/]+)(\/[^\/]*)?/", $uri, $out, PREG_SET_ORDER);
		if (count($out) == 0){
			return "";
		}
		return $out[0][0];
	}
}
?>