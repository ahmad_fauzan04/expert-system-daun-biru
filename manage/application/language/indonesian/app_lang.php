<?php

$lang['lbl_main_category'] = "Kategori Utama";
$lang['lbl_category_title'] = "Judul Kategori";
$lang['lbl_category_form'] = "Tambah / Ubah Kategori";
$lang['lbl_category_description'] = "Deskripsi Kategori";
$lang['lbl_thread'] = "Diskusi";
$lang['lbl_post'] = "Post";
$lang['lbl_reply'] = "Balasan";
$lang['lbl_thread_title'] = 'Judul Diskusi';
$lang['lbl_thread_description'] = 'Isi Pesan Diskusi';
$lang['lbl_reply_title'] = 'Judul Balasan';
$lang['lbl_reply_description'] = 'Isi Pesan Balasan';

$lang['def_mail_thread_start'] = '{thread_user} telah memulai diskusi berjudul "{thread_title}".
								Segera kunjungi {thread_url} untuk ikut berdiskusi.';
$lang['def_mail_thread_reply'] = '{thread_user} ikut berdiskusi dalam "{thread_title}".
								Ikuti detail diskusi di {thread_url}.';