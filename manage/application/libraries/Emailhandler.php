<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emailhandler {
	var $CI, $mailer;

	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->library('email');
	}
	
	private function setup() {
		$config = Array(
					'protocol'	=> 'smtp',
					'smtp_host'	=> 'smtp.telkom.net',
					'smtp_port'	=> 25,
					'mailtype'  => 'html'
				);
		
		$this->CI->email->initialize($config);
	}
	
	public function send($param) {
		log_message('debug', get_class().'.'.__FUNCTION__.'> arguments: '.print_r($param, true));
		$this->setup();

		$this->CI->email->from('notification@priangan.org', 'IPJ | Informasi Pangan Jakarta');
		$this->CI->email->to($param['to']);
		$this->CI->email->subject($param['subject']);
		$this->CI->email->message($param['message']);

		if (array_key_exists('sender_mail', $param))
			$this->CI->email->reply_to($param['sender_mail']);
		
		$status = $this->CI->email->send();
		$result = Array(
						'status' => $status,
						'message' => $this->CI->email->print_debugger()
					);
					
		if (!$status)
			log_message('error', 'mail send debug: '.$result['message']);
		
		return $result;
	}
	
}
?>
