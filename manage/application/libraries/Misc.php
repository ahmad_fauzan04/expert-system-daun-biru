<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Miscellaneous class
 *
 * This class provided several functions may be needed by the system
 *
 * @author Zakka Fauzan Muhammad
 */

 class Misc{
     function get_weeks($date, $rollover = 'Sunday', $to_rome = true){
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $i = 1;
        $weeks = 1;

        for($i; $i<=$elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if($day == strtolower($rollover) && $i > 1)  $weeks ++;
        } /*$weeks--*/;

        return $to_rome? $this->rome($weeks): $weeks;
    }

    function rome($num) {
         // Make sure that we only use the integer portion of the value
         $n = intval($num);
         $result = '';

         // Declare a lookup array that we will use to traverse the number:
         $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
         'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
         'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

         foreach ($lookup as $roman => $value)
         {
             // Determine the number of matches
             $matches = intval($n / $value);

             // Store that many characters
             $result .= str_repeat($roman, $matches);

             // Substract that from the number
             $n = $n % $value;
         }

         // The Roman numeral should be built, return it
         return $result;
    }

    /**
     * This function is incomplete
     *
     * A function to get the last date of a given range starting from 
     * a specified date
     *
     * @author Mark Muhammad
     * @param string $date_str date string in 'yyyy-mm-dd' format
     * @param string $range from constants RANGE_DAILY, RANGE_WEEKLY, 
     *      RANGE_MONTHLY, RANGE_YEARLY
     */
    public function get_last_date($date_str, $range)
    {
        switch ($range) {
            case RANGE_YEARLY:
                $last_date = date('Y', strtotime($date_str)).'-12-31';
                break;
            case RANGE_MONTHLY:
                $last_date = date('Y-m-t', strtotime($date_str));
                break;
            case RANGE_WEEKLY:
                $last_date = $date_str;
                break;
            default:
                $last_date = $date_str;
                break;
        }
    }

    public function get_shown_date($date_str, $range)
    {
        log_message('debug', get_class().'.'.__FUNCTION__."> date = $date_str | range = $range");
        $daylen = 86400;

        switch ($range) {
            case RANGE_YEARLY:
                $shown_date = date('Y', strtotime($date_str)).'-01-01';
                break;
            case RANGE_MONTHLY:
                $shown_date = date('Y-m-', strtotime($date_str)).'01';
                break;
            case RANGE_WEEKLY:
                $timestamp     = strtotime($date_str);
                $today         = strtotime(date('Y-m-d'));
                $day_iteration = 0;
                $day;
                log_message('debug', get_class().'.'.__FUNCTION__."> today's timestamp: $today");
                do {
                    $current    = $timestamp + ($daylen * $day_iteration++);
                    $shown_date = date('Y-m-d', $current);
                    $day        = strtolower(date('w', $current));
                    log_message('debug', get_class().'.'.__FUNCTION__."> current date: $shown_date");
                    log_message('debug', get_class().'.'.__FUNCTION__."> current timestamp: $current");
                } while ($current < $today && $day != 5);
                break;
            default:
                $shown_date = $date_str;
                break;
        }

        log_message('debug', get_class().'.'.__FUNCTION__."> $range => shown date = $shown_date");
        return $shown_date;
    }
 }
