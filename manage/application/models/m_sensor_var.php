<?php
class M_sensor_var extends CI_Model {
	private $id_sensor_group, $state, $question, $id_sensor_var;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function set_id_sensor_group($id){
		$this->id_sensor_group = $id;
		return $this;
	}
	
	public function set_state($id){
		$this->state = $id;
		return $this;
	}
	
	public function set_question($id){
		$this->question = $id;
		return $this;
	}
	
	public function set_id_sensor_var($id){
		$this->id_sensor_var = $id;
		return $this;
	}
	
	public function insert(){
		$rec['id_sensor_group'] = $this->id_sensor_group;
		$rec['state'] = $this->state;
		$rec['question'] = $this->question;
		
		return $this->db->insert('monita_sensor_var', $rec);
	}
	
	public function delete (){
		return $this->db->where('id_sensor_var', $this->id_sensor_var)->delete('monita_sensor_var');
	}
	
	public function get_all(){
		if (!empty($this->id_sensor_group) && !empty($this->state)){
			return $this->db->where('id_sensor_group', $this->id_sensor_group)->where('state', $this->state)->order_by('question', 'asc')->get('monita_sensor_var')->result();
		}
		return $this->db->order_by("question", "asc")->get('monita_sensor_var')->result();
	}
	
	public function update(){
		$rec = array();
		$rec['question'] = $this->question;
		
		return $this->db->where('id_sensor_var', $this->id_sensor_var)->update('monita_sensor_var', $rec);
	}
}
?>