<?php
class M_sensor_recommendation extends CI_Model {
	private $id_sensor_var, $recommendation, $id_sensor_recommendation;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function set_id_sensor_var ($id){
		$this->id_sensor_var = $id;
		return $this;
	}
	public function set_recommendation ($id){
		$this->recommendation = $id;
		return $this;
	}
	public function set_id_sensor_recommendation ($id){
		$this->id_sensor_recommendation = $id;
		return $this;
	}
	
	public function insert(){
		$rec['id_sensor_var'] = $this->id_sensor_var;
		$rec['recommendation'] = $this->recommendation;
		
		return $this->db->insert('monita_sensor_recommendation', $rec);
	}
	
	public function delete (){
		return $this->db->where('id_sensor_recommendation', $this->id_sensor_recommendation)->delete('monita_sensor_recommendation');
	}
	
	public function update(){
		$rec['recommendation'] = $this->recommendation;
		
		return $this->db->where('id_sensor_recommendation', $this->id_sensor_recommendation)->update('monita_sensor_recommendation', $rec);
	}
	
	public function get_all(){
		if (!empty($this->id_sensor_var)){
			return $this->db->where('id_sensor_var', $this->id_sensor_var)->order_by('recommendation', 'asc')->get('monita_sensor_recommendation')->result();
		}
		return $this->db->order_by("recommendation", "asc")->get('monita_sensor_recommendation')->result();
	}
}
?>