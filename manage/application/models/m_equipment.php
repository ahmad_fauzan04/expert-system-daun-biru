<?php
class M_equipment extends CI_Model {
	private $id, $nama_equipment;
	
	public function __construct(){
		parent::__construct();
	}
	public function get_all(){
		return $this->db->order_by("nama_equipment", "asc")->get('monita_equipment')->result();
	}
}
?>