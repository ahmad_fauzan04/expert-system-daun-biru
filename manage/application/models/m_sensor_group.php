<?php
class M_sensor_group extends CI_Model {
	private $id_equipment, $name, $id_sensor_group, $high_limit, $low_limit;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function set_id_equipment($id){
		$this->id_equipment = $id;
		return $this;
	}
	public function set_name($id){
		$this->name = $id;
		return $this;
	}
	public function set_id_sensor_group($id){
		$this->id_sensor_group = $id;
		return $this;
	}
	public function set_high_limit($id){
		$this->high_limit = $id;
		return $this;
	}
	public function set_low_limit($id){
		$this->low_limit = $id;
		return $this;
	}
	
	public function get_group_list(){
		$data = $this->db->select('sg.id_sensor_group, sg.name, s.id_sensor, s.id_titik_ukur, tu.nama_titik');
		
		if (!empty($this->id_equipment)){$data = $data->where('sg.id_equipment', $this->id_equipment);}
		
		return $data
		->from('monita_sensor_group sg')
		->join('monita_sensor s', 'sg.id_sensor_group = s.id_sensor_group')
		->join('monita_titik_ukur tu', 'tu.id_titik = s.id_titik_ukur')
		->order_by('sg.name asc, tu.nama_titik asc')->get()->result();
	}
	
	public function get_all(){
		if (!empty($this->id_equipment)){
			return $this->db->where('id_equipment', $this->id_equipment)->order_by('name', 'asc')->get('monita_sensor_group')->result();
		}
		return $this->db->order_by("name", "asc")->get('monita_sensor_group')->result();
	}
	
	public function insert(){
		$rec['id_equipment'] = $this->id_equipment;
		$rec['name'] = $this->name;
		$rec['high_limit'] = $this->high_limit;
		$rec['low_limit'] = $this->low_limit;
		
		return $this->db->insert('monita_sensor_group', $rec);
	}
	
	public function delete(){
		$rec['id_sensor_group'] = $this->id_sensor_group;
		
		return $this->db->where('id_sensor_group', $this->id_sensor_group)->delete('monita_sensor_group');
	}
	
	public function update(){
		$rec = array();
		if (!empty($this->name)){
			$rec['name'] = $this->name;
		}
		if (!empty($this->high_limit)){
			$rec['high_limit'] = $this->high_limit;
		}
		if (!empty($this->low_limit)){
			$rec['low_limit'] = $this->low_limit;
		}
		
		return $this->db->where('id_sensor_group', $this->id_sensor_group)->update('monita_sensor_group', $rec);
	}
	
	public function get_related (){
		$data = $this->db->select('sg.id_sensor_group,sg.name,sg.low_limit,sg.high_limit,sv.state,sv.id_sensor_var,sv.question,sr.id_sensor_recommendation,sr.recommendation');
		
		if (!empty($this->id_equipment)){
			$data->where('sg.id_equipment', $this->id_equipment);
		}
		
		return $data->from('monita_sensor_group sg')
		->join('monita_sensor_var sv', 'sg.id_sensor_group = sv.id_sensor_group', 'left')
		->join('monita_sensor_recommendation sr', 'sv.id_sensor_var = sr.id_sensor_var', 'left')
		->order_by('sg.id_sensor_group desc, sv.state asc, sv.id_sensor_var asc, sr.id_sensor_recommendation asc')->get()->result();
	}
}
?>