<?php
class M_Sensor extends CI_Model {
	private $id_equipment, $id_titik_ukur, $id_sensor_group, $id_sensor;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function set_id_equipment($id){
		$this->id_equipment = $id;
		return $this;
	}
	public function set_id_titik_ukur($id){
		$this->id_titik_ukur = $id;
		return $this;
	}
	public function set_id_sensor_group($id){
		$this->id_sensor_group = $id;
		return $this;
	}
	public function set_id_sensor($id){
		$this->id_sensor = $id;
		return $this;
	}
	
	public function delete (){
		return $this->db->where('id_sensor', $this->id_sensor)->delete('sensor');
	}
	
	public function get_composite(){
		$data = $this->db->select('eq.id_equipment, eq.nama_equipment, sg.id_sensor_group, sg.name, sv.id_sensor_var, sv.state, sv.question, sr.id_sensor_recommendation, sr.recommendation');
		
		if (!empty($this->id_equipment)){$data = $data->where('eq.id_equipment', $this->id_equipment);}
		
		return $data
		->from('monita_equipment eq')
		->join('monita_sensor_group sg', 'eq.id_equipment = sg.id_equipment')
		->join('monita_sensor_var sv', 'sg.id_sensor_group = sv.id_sensor_group')
		->join('monita_sensor_recommendation sr', 'sv.id_sensor_var = sr.id_sensor_var')
		->order_by('eq.id_equipment asc, id_sensor_group asc, state asc, id_sensor_var asc')->get()->result();
	}
	
	public function get_sensor_list(){
		if (!empty($this->id_equipment)){return $this->db->where('id_equipment', $this->id_equipment)->get('titik_ukur')->result();}
		
		return $this->db->get('monita_titik_ukur')->result();
	}
	
	public function get_composite_question(){
		$data = $this->db->select('eq.id_equipment, eq.nama_equipment, sg.id_sensor_group, sg.name, sv.id_sensor_var, sv.state, sv.question');
		
		if (!empty($this->id_equipment)){$data = $data->where('eq.id_equipment', $this->id_equipment);}
		
		return $data
		->from('monita_equipment eq')
		->join('monita_sensor_group sg', 'eq.id_equipment = sg.id_equipment')
		->join('monita_sensor_var sv', 'sg.id_sensor_group = sv.id_sensor_group')
		->order_by('eq.id_equipment asc, id_sensor_group asc, state asc, id_sensor_var asc')->get()->result();
	}
	
	public function insert(){
		$rec['id_titik_ukur'] = $this->id_titik_ukur;
		$rec['id_sensor_group'] = $this->id_sensor_group;
		
		return $this->db->insert('monita_sensor', $rec);
	}
}
?>