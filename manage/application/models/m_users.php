<?php
class M_users extends CI_Model {
	private $id, $username, $password;
	private $hash = FALSE;
	
	public function __construct(){
		parent::__construct();
		$this->load->library('encrypt');
	}
	
	public function set_id ($id){
		$this->id_user = $id;
		return $this;
	}
	
	public function set_username ($username){
		$this->username = $username;
		return $this;
	}
	
	public function set_password ($pass){
		$this->password = $pass;
		if ($this->hash)
			$this->password = $this->encrypt->sha1('pengasin'.$pass.'garam...');
		return $this;
	}
	
	public function insert(){
		$data = array('username' => $this->username, 'password' => $this->password);
		$this->db->insert('monita_users', $data);
		
		return $this->db->insert_id();
	}
		
	public function login(){
		$this -> db -> select('id_user, username');
		$this -> db -> from('monita_users');
		$this -> db -> where('username', $this->username);
		$this -> db -> where('password', md5($this->password));
		$this -> db -> limit(1);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	public function get_all(){
		return $this->db->get('monita_users')->result();
	}
}
?>