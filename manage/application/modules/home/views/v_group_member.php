<div id="content" class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<div class="row">
					<div class="col-md-12">
						<select id="eq_id">
						<?php foreach ($equipment as $eq){?>
							<option value="<?php echo $eq->id_equipment; ?>" <?php echo ($eq->id_equipment==$eq_id)?'selected':'';?>><?php echo $eq->nama_equipment; ?></option>
						<?php } ?>
						<select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="add-recommendation">
							<select id="add-group-id" style="width:200px;">
								<option value=0>--select group--</option>
							<?php foreach ($sensor_group as $sg){?>
								<option value="<?php echo $sg->id_sensor_group; ?>"><?php echo $sg->name; ?></option>
							<?php } ?>
							</select>
							<select id="add-titik-ukur-id">
								<option value=0>--select sensor--</option>
							<?php foreach ($sensor_list as $sl){?>
								<option value="<?php echo $sl->id_titik; ?>"><?php echo $sl->nama_titik." -- ".$sl->id_titik; ?></option>
							<?php } ?>
							</select>
							<input type="button" id="tambah-sensor" value="+sensor">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="text" id="add-group" style="width:200px;">
						<input type="text" id="low-limit" placeholder="low limit" style="width:100px;">
						<input type="text" id="high-limit" placeholder="high limit" style="width:100px;">
						<input type="button" id="tambah-group" value="+group">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="content-post">
							<div class="media-list log-list">
								<table id="list">
								<?php foreach ($group_list as $gl){ ?>
									<tr class="media media-border"  data-id="<?php echo $gl->id_sensor; ?>">
										<?php echo '<td>'.$gl->name.'</td><td>'.$gl->nama_titik.'</td><td><a class="delete-sensor" href="#" data-id="'.$gl->id_sensor.'">delete<a></td>'; ?>
									</tr>
								<?php } ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var base_url = "<?=base_url(); ?>";
	refresh_list();
	
	$('#eq_id').change(function(){
		refresh_sensor();
		refresh_group();
		refresh_list();
	});
	
	$('#tambah-sensor').click(function(){
		add_sensor();
	});
	$('#tambah-group').click(function(){
		add_group();
	});
	$('.delete-sensor').click(function(e){
		e.preventDefault();
		delete_sensor(this)
	});
	
	
	function delete_sensor(el){
		var id = $(el).attr('data-id');
		$.ajax({
			url: base_url+'api/sensor/delete',
			dataType: 'json',
			data: {"id": id},
			method: 'POST',
			success: function(resp){
				if(resp.status == 'success'){
					refresh_list();
					return true;
				}
				return false;
			}
		});
	}
	
	function add_group(){
		var eq_id = $('#eq_id option:selected').attr('value');
		var group = $('#add-group').val();
		var high_limit = $('#high-limit').val();
		var low_limit = $('#low-limit').val();
		if( eq_id != 0 && group != '' && high_limit != '' && low_limit != ''){
			$.ajax({
				url: base_url+'api/sensor_group/save',
				dataType: 'json',
				data: {"eq_id": eq_id, "group": group, "low_limit": low_limit, "high_limit": high_limit},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_group();
						$('#add-group').val('');
						$('#high-limit').val('');
						$('#low-limit').val('');
						console.log('success');
					}
				}
			});
		}
	}
	
	function add_sensor(){
		var id_sg = $('#add-group-id option:selected').attr('value');
		var id_tu = $('#add-titik-ukur-id option:selected').attr('value');
		console.log(id_tu);
		if( id_tu != 0 && id_sg != null){
			$.ajax({
				url: base_url+'api/sensor/save',
				dataType: 'json',
				data: {"id_tu": id_tu, "id_sg": id_sg},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$('#recommendation').val('');
						refresh_list();
						console.log('success');
					}
				}
			});
		}
	}
	
	function refresh_sensor(){
		$('#add-titik-ukur-id').html('<option value=0>--select sensor--</option>');
		if ($('#eq_id option:selected').attr('value') != 0){
			$.ajax({
				url: base_url+'api/sensor/get_sensor_list',
				dataType: 'json',
				data: {"eq_id": $('#eq_id option:selected').attr('value')},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						temp = resp.data;
						$.each(resp.data, function(key, value){
							$('#add-titik-ukur-id').append('<option value="'+value.id_titik+'">'+value.nama_titik+' -- '+value.id_titik+'</option>');
						});
					}
				}
			});
		}
	}
	
	function refresh_group(){
		$('#add-group-id').html('<option value=0>--select group--</option>');
		$('#add-var').html('<option value=0>--select question--</option>');
		if ($('#eq_id option:selected').attr('value') !== 0){
			$.ajax({
				url: base_url+'api/sensor_group/get',
				dataType: 'json',
				data: {"eq_id": $('#eq_id option:selected').attr('value')},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						temp = resp.data;
						$.each(resp.data, function(key, value){
							$('#add-group-id').append('<option value="'+value.id_sensor_group+'">'+value.name+'</option>');
						});
					}
				}
			});
		}
	}
	
	function refresh_list(){
		$('#list').html('');
		if ($('#eq_id option:selected').attr('value') !== 0){
			$.ajax({
				url: base_url+'api/sensor/get_group_list',
				dataType: 'json',
				data: {"eq_id": $('#eq_id option:selected').attr('value')},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						console.log(resp);
						$('#list').append('<tr><th>Grup Sensor</th><th>Nama Sensor</th><th></th></tr>');
						$.each(resp.data, function(key, value){
							$('#list').append('<tr class="media media-border"  data-id="'+value.id_sensor+'"><td>'+value.name+'</td><td>'+value.nama_titik+'</td><td><a class="delete-sensor" href="#" data-id="'+value.id_sensor+'">delete<a></td></tr>');
						});
						$('.delete-sensor').click(function(e){
							e.preventDefault();
							delete_sensor(this)
						});
					}
				}
			});
		}
	}
});
</script>