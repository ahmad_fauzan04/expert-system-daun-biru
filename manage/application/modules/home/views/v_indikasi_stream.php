<div id="content" class="container">
	<div class="row">
		<input type="hidden" id="eq_id" value="1">
		<input type="text" id="add-group" placeholder="Tambah Grup Sensor" class="col-md-6">
		<input type="text" id="low-limit" placeholder="Batas Bawah" class="col-md-2">
		<input type="text" id="high-limit" placeholder="Batas Atas" class="col-md-2">
		<input type="button" id="tambah-group" value="+" class="col-md-2">
	</div>
	<div class="row">
		<div class="col-md-12 grup-cardlist">
			
			<?php
				$i = count($list);
				if ($i<0){
					echo "Belum ada data";
				}else{
					$ar = array();
					foreach ($list as $l){
						$ar[$l->id_sensor_group]['name'] = $l->name;
						$ar[$l->id_sensor_group]['high_limit'] = $l->high_limit;
						$ar[$l->id_sensor_group]['low_limit'] = $l->low_limit;
						$ar[$l->id_sensor_group][$l->state][$l->id_sensor_var]['question'] = $l->question;
						$ar[$l->id_sensor_group][$l->state][$l->id_sensor_var]['rec'][$l->id_sensor_recommendation] = $l->recommendation;
					}
					
					foreach ($ar as $i=>$a){
			?>
			<div class="row group-id" id-val="<?=$i; ?>">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-9 grup-name">
							<span class="value"><?=$a['name'];?></span>
							<span class="edit hide">
								<input type="text" class="el-value" value="<?=$a['name'];?>">
								<span class="edit-group">v</span>
								<span class="cancel-edit">x</span>
							</span>
						</div>
						<div class="col-md-2">
							<span class="value"><?=$a['low_limit'];?> - <?=$a['high_limit'];?></span>
							<span class="edit hide">
								<input type="text" class="low-limit" value="<?=$a['low_limit'];?>"> - <input type="text" class="high-limit" value="<?=$a['high_limit'];?>">
								<span class="edit-limit">v</span>
								<span class="cancel-edit">x</span>
							</span>
						</div>
						<div class="col-md-1 delete-group">x</div>
					</div>
					<div class="row">
						<div class="col-md-12">
						<?php 
							$state = array(array('group_high', 'Grup High'), array('group_low', 'Grup Low'), array('sensor_high', 'Sensor High'), array('sensor_low','Sensor Low'));
							foreach($state as $s){
							?>
							<div class="row">
								<div class="col-md-12 grup-state"><?=$s[1]; ?></div>
							</div>
							<div class="row">
								<div class="col-md-12 grup-indikasi" id-val="<?=$s[0]; ?>">
								<?php
								if (!empty($a[$s[0]])){
									foreach ($a[$s[0]] as $j=>$gh){ ?>
									<div class="row var-id" id-val=<?=$j; ?>>
										<div class="col-md-5">
											<span class="value"><?=$gh['question']?></span>
											<span class="edit hide">
												<input class="el-value" type="text" value="<?=$gh['question']?>">
												<span class="edit-indikasi">v</span>
												<span class="cancel-edit">x</span>
											</span>
										</div>
										<div class="col-md-1 delete-indikasi">x</div>
										<div class="col-md-6 grup-rekomendasi">
										<?php foreach ($gh['rec'] as $k=>$rec){ if (!empty($rec)){ ?>
											<div class="row rec-id" id-val="<?=$k; ?>">
												<div class="col-md-11">
													<span class="value"><?=$rec; ?></span>
													<span class="edit hide">
														<input class="el-value" type="text" value="<?=$rec; ?>">
														<span class="edit-rekomendasi">v</span>
														<span class="cancel-edit">x</span>
													</span>
												</div>
												<div class="col-md-1 delete-rekomendasi">x</div>
											</div>
										<?php }}?>
											<div class="row add">
												<input type="text" placeholder="tambah rekomendasi" class="col-md-11 add-rekomendasi">
												<input type="button" value="+" class="tambah-rekomendasi">
											</div>
										</div>
									</div>
								<?php
									}
								}?>
									
									<div class="row add">
										<input type="text" class="col-md-11 add-indikasi" placeholder="tambah indikasi">
										<input type="button" value="+" class="col-md-1 tambah-indikasi">
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php
					}
				}
			?>
			
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var base_url = "<?=base_url(); ?>";
	var current_url = base_url+'home/indikasi?eq_id='+$('#eq_id option:selected').attr('value');
	
	$('#eq_id').change(function(){
		window.location=base_url+'home/indikasi?eq_id='+$('#eq_id option:selected').attr('value');
	});
	
	$('#tambah-group').click(function(){
		add_group();
	});
	
	$('.delete-group').click(function(){
		delete_group(this);
	});
	
	$('.tambah-indikasi').click(function(){
		add_indikasi(this);
	});
	
	$('.delete-indikasi').click(function(){
		delete_indikasi(this);
	});
	
	$('.tambah-rekomendasi').click(function(){
		add_rekomendasi(this);
	});
	
	$('.delete-rekomendasi').click(function(){
		delete_rekomendasi(this);
	});
	
	$('.value').click(function(){
		//var $p = $(this).parent();
		$(this).addClass('hide');
		$(this).siblings('.edit').removeClass('hide').find('input:first').focus();
		
		$('.cancel-edit').click(function(){
			$(this).parent().addClass('hide');
			$(this).parent().siblings('.value').removeClass('hide');
		});
	});
	
	$('.edit-group').click(function(){
		edit_group(this);
	});
	
	$('.edit-limit').click(function(){
		edit_limit(this);
	});
	
	$('.edit-indikasi').click(function(){
		edit_indikasi(this);
	});
	
	$('.edit-rekomendasi').click(function(){
		edit_rekomendasi(this);
	});
	
	function edit_group(ev){
		var group_id = $(ev).parents('.group-id').attr('id-val'),
			val = $(ev).siblings('.el-value').val();
			
		if (group_id != 0 && val!=''){
			$.ajax({
				url: base_url+'api/sensor_group/edit',
				dataType: 'json',
				data: {'id': group_id, 'name': val},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$(ev).parent().siblings('.value').html(val);
						$(ev).parent().addClass('hide');
						$(ev).parent().siblings('.value').removeClass('hide');
					}
				}
			});
		}
	}
	
	function edit_limit(ev){
		var group_id = $(ev).parents('.group-id').attr('id-val'),
			low_val = $(ev).siblings('.low-limit').val(),
			high_val = $(ev).siblings('.high-limit').val();
			
		if (group_id != 0){
			$.ajax({
				url: base_url+'api/sensor_group/edit',
				dataType: 'json',
				data: {'id': group_id, 'high_limit': high_val, 'low_limit': low_val},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$(ev).parent().siblings('.value').html(low_val+' - '+high_val);
						$(ev).parent().addClass('hide');
						$(ev).parent().siblings('.value').removeClass('hide');
					}
				}
			});
		}
	}
	
	function add_group(){
		var eq_id = $('#eq_id').val();
		var group = $('#add-group').val();
		var high_limit = $('#high-limit').val();
		var low_limit = $('#low-limit').val();
		if( eq_id != 0 && group != '' && high_limit != '' && low_limit != ''){
			$.ajax({
				url: base_url+'api/sensor_group/save',
				dataType: 'json',
				data: {"eq_id": eq_id, "group": group, "low_limit": low_limit, "high_limit": high_limit},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$('#add-group').val('');
						$('#high-limit').val('');
						$('#low-limit').val('');
						console.log('tambah grup');
						window.location=current_url;
					}
				}
			});
		}
	}
	
	function delete_group(e){
		var group_id = $(e).closest('.group-id').attr('id-val');
		if (confirm('hapus grup?')){
			$.ajax({
				url: base_url+'api/sensor_group/delete',
				dataType: 'json',
				data: {"id": group_id},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_list();
					}
				}
			});
		}
	}
	
	function edit_indikasi(ev){
		var var_id = $(ev).parents('.var-id').attr('id-val'),
			val = $(ev).siblings('.el-value').val();
			
		if (var_id != 0 && val!=''){
			$.ajax({
				url: base_url+'api/sensor_var/edit',
				dataType: 'json',
				data: {'id_sensor_var': var_id, 'question': val},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$(ev).parent().siblings('.value').html(val);
						$(ev).parent().addClass('hide');
						$(ev).parent().siblings('.value').removeClass('hide');
					}
				}
			});
		}
	}
	
	function add_indikasi(e){
		var group_id = $(e).closest('.group-id').attr('id-val');
		var state = $(e).closest('.grup-indikasi').attr('id-val');
		var ind = $(e).siblings('.add-indikasi').val();
		
		
		if( group_id != 0 && state != 0 && ind != ''){
			$.ajax({
				url: base_url+'api/sensor_var/save',
				dataType: 'json',
				data: {"group_id": group_id, "state": state, "question": ind},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_list();
						console.log('tambah indikasi');
					}
				}
			});
		}
		
	}
	
	function delete_indikasi(e){
		var var_id = $(e).closest('.var-id').attr('id-val');
		if (confirm('hapus indikasi?')){
			$.ajax({
				url: base_url+'api/sensor_var/delete',
				dataType: 'json',
				data: {"id": var_id},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_list();
					}
				}
			});
		}
	}
	
	function edit_rekomendasi(ev){
		var rec_id = $(ev).parents('.rec-id').attr('id-val'),
			val = $(ev).siblings('.el-value').val();
			
		if (rec_id != 0 && val!=''){
			$.ajax({
				url: base_url+'api/sensor_recommendation/edit',
				dataType: 'json',
				data: {'id': rec_id, 'rec': val},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$(ev).parent().siblings('.value').html(val);
						$(ev).parent().addClass('hide');
						$(ev).parent().siblings('.value').removeClass('hide');
					}
				}
			});
		}
	}
	
	function add_rekomendasi(e){
		var var_id = $(e).closest('.var-id').attr('id-val');
		var rec = $(e).siblings('.add-rekomendasi').val();
		if( var_id != 0 && rec != ''){
			$.ajax({
				url: base_url+'api/sensor_recommendation/save',
				dataType: 'json',
				data: {"var_id": var_id, "rec": rec},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$('#recommendation').val('');
						refresh_list();
						console.log('tambah rekomendasi');
					}
				}
			});
		}
	}
	
	function delete_rekomendasi(e){
		var rec_id = $(e).closest('.rec-id').attr('id-val');
		if (confirm('hapus rekomendasi?')){
			$.ajax({
				url: base_url+'api/sensor_recommendation/delete',
				dataType: 'json',
				data: {"id": rec_id},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_list();
					}
				}
			});
		}
	}
	
	function refresh_list(){
		$('.grup-cardlist').html('');
		if ($('#eq_id option:selected').attr('value') !== 0){
			window.location=current_url;
			/*
			$.ajax({
				url: base_url+'api/sensor_group/get_related',
				dataType: 'json',
				data: {'eq_id': $('#eq_id option:selected').attr('value')},
				method: 'GET',
				success: function(resp){
					console.log('hoho');
				}
			});
			*/
		}
	}
});
</script>