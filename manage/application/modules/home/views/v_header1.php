<html>
<head>
	<title>Smart System Management</title>
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/fui/css/flat-ui.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/datepicker.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/style1.css">
	<script type="text/javascript" src="<?=base_url();?>assets/js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/date.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/autosize.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/flatui-checkbox.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/flatui-radio.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery.tagsinput.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/bootstrap-typeahead.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/application.js"></script>
</head>
<body>
<div id="header">
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-collapse-03"></button>
	          	<a href="#" class="navbar-brand">Smart System Management</a>
	        </div>
	        <div class="navbar-collapse collapse navbar-collapse-03">
	        	<ul class="nav navbar-nav navbar-right">
					<li><a href="<?=base_url();?>home/indikasi">Indikasi</a></li>
					<li class="divider"></li>
                  	<li class="dropdown">
                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      		<?=$user['username']; ?><b class="caret"></b>
                    	</a>
                    	<ul class="dropdown-menu">
                      		<li><a href="<?=base_url();?>home/logout">Logout</a></li>
                    	</ul>
                  	</li>
                </ul>
	        </div>
	    </div>
    </div>
</div>