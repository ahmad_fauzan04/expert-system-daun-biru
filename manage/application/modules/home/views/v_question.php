hahaha<div id="content" class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<div class="row">
					<div class="col-md-12">
						<select id="eq_id">
						<?php foreach ($equipment as $eq){?>
							<option value="<?php echo $eq->id_equipment; ?>" <?php echo ($eq->id_equipment==$eq_id)?'selected':'';?>><?php echo $eq->nama_equipment; ?></option>
						<?php } ?>
						<select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="add-question">
							<select id="add-group-id">
								<option value=0>--select group--</option>
							<?php foreach ($sensor_group as $sg){?>
								<option value="<?php echo $sg->id_sensor_group; ?>"><?php echo $sg->name; ?></option>
							<?php } ?>
							</select>
							<select id="add-state">
								<option value=0>--select state--</option>
								<option value="group_high">group high</option>
								<option value="group_low">group low</option>
								<option value="sensor_high">sensor high</option>
								<option value="sensor_low">sensor low</option>
							</select>
							<input type="text" id="question">
							
							<input type="button" id="tambah-pertanyaan" value="+pertanyaan">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="content-post">
							<div class="media-list log-list">
								<table id="list">
								<?php foreach ($sensor_var as $l){ ?>
									<tr class="media media-border"  data-id="<?php echo $l->id_sensor_recommendation; ?>">
										<?php echo '<td>'.$l->name.'</td><td>'.$l->state.'</td><td>'.$l->question.'</td><td><a class="delete_var" href="#" data-id="'.$l->id_sensor_var.'">delete</a></td>'; ?>
									</tr>
								<?php } ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var base_url = "<?=base_url(); ?>";
	refresh_list();
	
	$('#eq_id').change(function(){
		refresh_group();
		refresh_list();
	});
	
	$('#add-group-id').change(function(){refresh_list();});
	$('#add-state').change(function(){refresh_list();});
	$('#tambah-rekomendasi').click(function(){
		add_recommendation();
	});
	$('#tambah-pertanyaan').click(function(){
		add_question();
		refresh_list();
	});
	$('.delete_var').click(function(e){
		e.preventDefault();
		delete_question(this)
		console.log('cobain');
	});
	
	function delete_question(el){
		var id = $(el).attr('data-id');
		$.ajax({
			url: base_url+'api/sensor_var/delete',
			dataType: 'json',
			data: {"id": id},
			method: 'POST',
			success: function(resp){
				if(resp.status == 'success'){
					refresh_list();
					return true;
				}
				return false;
			}
		});
	}
	
	function add_question(){
		var group_id = $('#add-group-id option:selected').attr('value');
		var state = $('#add-state option:selected').attr('value');
		var rec = $('#question').val();
		if( group_id != 0 && state != 0 && rec != ''){
			$.ajax({
				url: base_url+'api/sensor_var/save',
				dataType: 'json',
				data: {"group_id": group_id, "state": state, "question": rec},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_list();
						$('#recommendation').val('');
					}
				}
			});
		}
	}
	
	function refresh_group(){
		$('#add-group-id').html('<option value=0>--select group--</option>');
		$('#add-var').html('<option value=0>--select question--</option>');
		if ($('#eq_id option:selected').attr('value') !== 0){
			$.ajax({
				url: base_url+'api/sensor_group/get',
				dataType: 'json',
				data: {"eq_id": $('#eq_id option:selected').attr('value')},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						temp = resp.data;
						$.each(resp.data, function(key, value){
							$('#add-group-id').append('<option value="'+value.id_sensor_group+'">'+value.name+'</option>');
						});
					}
				}
			});
		}
	}
	
	function refresh_list(){
		$('#list').html('');
		if ($('#eq_id option:selected').attr('value') !== 0){
			$.ajax({
				url: base_url+'api/sensor/get_question_list',
				dataType: 'json',
				data: {"eq_id": $('#eq_id option:selected').attr('value')},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						temp = resp.data;
						$('#list').append('<tr><th>Grup Sensor</th><th>State</th><th>Indikasi</th><th></th></tr>');
						$.each(resp.data, function(key, value){
							$('#list').append('<tr class="media media-border"  data-id="'+value.id_sensor_recommendation+'"><td>'+value.name+'</td><td>'+value.state+'</td><td>'+value.question+'</td><td><a class="delete_recom" href="#" data-id="'+value.id_sensor_var+'">delete</a></td></tr>');
						});
						$('.delete_recom').click(function(e){
							e.preventDefault();
							if (delete_question(this)){
								refresh_list();
							}
							
						});
					}
				}
			});
		}
	}
});
</script>