<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="absolute-center">
					<form class="form-horizontal" role="form" method="POST" action="">
						<div class="form-group">
							<label for="InputUsername" class="col-sm-3 control-label">Username</label>
							<div class="col-sm-8">
								<input type="text" class="form-control input-lg" id="InputUsername" name="username">
							</div>
						</div>
						<div class="form-group">
							<label for="InputPassword" class="col-sm-3 control-label">Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control input-lg" id="InputPassword" name="password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-8">
								<button type="submit" class="btn btn-lg btn-block btn-primary">Login</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
