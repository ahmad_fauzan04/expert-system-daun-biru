<div id="content" class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<div class="row">
					<div class="col-md-12">
						<select id="eq_id">
						<?php foreach ($equipment as $eq){?>
							<option value="<?php echo $eq->id_equipment; ?>" <?php echo ($eq->id_equipment==$eq_id)?'selected':'';?>><?php echo $eq->nama_equipment; ?></option>
						<?php } ?>
						<select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="add-recommendation">
							<input type="text" id="add-group" style="width:200px;">
							<input type="text" id="low-limit" placeholder="low limit" style="width:100px;">
							<input type="text" id="high-limit" placeholder="high limit" style="width:100px;">
							<input type="button" id="tambah-group" value="+group">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="content-post">
							<div class="media-list log-list">
								<table id="list">
								<?php foreach ($group_list as $gl){ ?>
									<tr class="media media-border"  data-id="<?php echo $gl->id_sensor_group; ?>">
										<?php echo '<td>'.$gl->name.'</td><td>'.$gl->low_limit.'</td><td>'.$gl->high_limit.'</td><td><a class="delete-group" href="#" data-id="'.$gl->id_sensor_group.'">delete<a></td>'; ?>
									</tr>
								<?php } ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var base_url = "<?=base_url(); ?>";
	refresh_list();
	
	$('#eq_id').change(function(){
		refresh_list();
	});
	$('#tambah-group').click(function(){
		add_group();
	});
	$('.delete-sensor').click(function(e){
		e.preventDefault();
		delete_group(this)
	});
	
	function delete_group(el){
		var id = $(el).attr('data-id');
		if (confirm('hapus data?')){
			$.ajax({
				url: base_url+'api/sensor_group/delete',
				dataType: 'json',
				data: {"id": id},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_list();
						return true;
					}
					return false;
				}
			});
		}
	}
	
	function add_group(){
		var eq_id = $('#eq_id option:selected').attr('value');
		var group = $('#add-group').val();
		var high_limit = $('#high-limit').val();
		var low_limit = $('#low-limit').val();
		if( eq_id != 0 && group != '' && high_limit != '' && low_limit != ''){
			$.ajax({
				url: base_url+'api/sensor_group/save',
				dataType: 'json',
				data: {"eq_id": eq_id, "group": group, "low_limit": low_limit, "high_limit": high_limit},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						refresh_group();
						$('#add-group').val('');
						$('#high-limit').val('');
						$('#low-limit').val('');
						console.log('success');
						refresh_list();
					}
				}
			});
		}
	}
	
	function refresh_group(){
		$('#add-group-id').html('<option value=0>--select group--</option>');
		$('#add-var').html('<option value=0>--select question--</option>');
		if ($('#eq_id option:selected').attr('value') !== 0){
			$.ajax({
				url: base_url+'api/sensor_group/get',
				dataType: 'json',
				data: {"eq_id": $('#eq_id option:selected').attr('value')},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						temp = resp.data;
						$.each(resp.data, function(key, value){
							$('#add-group-id').append('<option value="'+value.id_sensor_group+'">'+value.name+'</option>');
						});
					}
				}
			});
		}
	}
	
	function refresh_list(){
		$('#list').html('');
		if ($('#eq_id option:selected').attr('value') !== 0){
			$.ajax({
				url: base_url+'api/sensor_group/get',
				dataType: 'json',
				data: {"eq_id": $('#eq_id option:selected').attr('value')},
				method: 'POST',
				success: function(resp){
					if(resp.status == 'success'){
						$('#list').append('<tr><th>Grup Sensor</th><th>Batas Bawah</th><th>Batas Atas</th><th></th></tr>');
						$.each(resp.data, function(key, value){
							$('#list').append('<tr class="media media-border"  data-id="'+value.id_sensor_group+'"><td>'+value.name+'</td><td>'+value.low_limit+'</td><td>'+value.high_limit+'</td><td><a class="delete-group" href="#" data-id="'+value.id_sensor_group+'">delete<a></td>');
						});
						$('.delete-group').click(function(e){
							e.preventDefault();
							delete_group(this)
						});
					}
				}
			});
		}
	}
});
</script>