<?php
class Home extends CI_Controller {
	private $eq_id;
	function __construct(){
		parent::__construct();
	}
	
	function index(){
		$this->indikasi();
	}
	
	function indikasi(){
		//$this->eq_id = $this->input->get('eq_id')?$this->input->get('eq_id'):1;
		$this->authentication->restricted();
		$this->load->model(array('m_equipment', 'm_sensor_group'));
		
		//$isi['eq_id'] = $this->eq_id;
		$isi['list'] = $this->m_sensor_group->get_related();
		//$isi['equipment'] = $this->m_equipment->get_all();
		$profile['user']=$this->authentication->user_detail();
		
		$this->load->view('home/v_header1', $profile);
		$this->load->view('home/v_indikasi_stream', $isi);
		$this->load->view('home/v_footer');
	}
			
	function login(){
		$this->load->model(array('m_users'));
		$log = $this->session->userdata('logged_in');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if (empty($log) && $username){
		
			$result = $this->m_users->set_username($username)->set_password($password)->login();
			
			if ($result){
				$sess = array();
				foreach($result as $row){
					$sess = array(
						'id' => $row->id_user,
						'username' => $row->username,
						'logged_id' => true
					);
				}
				$this->session->set_userdata('logged_in', $sess);
				redirect('home/login', 'refresh');
			}
		}elseif ($log){
			redirect('home');
		}
		
		$this->load->view('header');
		$this->load->view('home/v_home');
		$this->load->view('footer');
	}
	
	function logout(){
		$this->authentication->restricted();
		$this->authentication->log_out();
		redirect('home/login');
	}
}
	
?>