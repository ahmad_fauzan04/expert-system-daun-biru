<?php
class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
	}
	
	function index(){
		$this->authentication->restricted();
		if ($this->input->post('action') == 'tambah'){
			$this->add_log();
		}elseif($this->input->post('action') == 'tambahKomentar'){
			$this->add_comment();
		}
		$profile['user']=$this->authentication->user_detail();
		$keluar['users']=$this->authentication->get_all();
		$keluar['log'] = $this->get_all_log();
		$keluar['user']=$this->authentication->user_detail();
		$this->load->view('header-logstream', $profile);
		$this->load->view('home/v_logstream', $keluar);
		$this->load->view('footer-logstream');
	}
	
	private function add_comment (){
		$this->load->model(array('m_comments'));
		$log_item_id = $this->input->post('log_item_id');
		$text = $this->input->post('text');
		$user_detail = $this->authentication->user_detail();
		$user_id = $user_detail['id'];
		$this->m_comments->set_log_item_id($log_item_id)->set_comment($text)->set_user_id($user_id)->insert();
	}

	private function add_log(){
		$this->load->model(array('m_hashtag', 'm_logs'));
		$text = $this->input->post('addLog');
		$tmpDate = explode('/', $this->input->post('tanggal'));
		$date = date('Y-m-d', mktime(0, 0, 0, $tmpDate[1], $tmpDate[0], $tmpDate[2]));
		$benarkah = TRUE; //flag bisa lanjut apa engga
		
	$by_line = explode("\r\n", $text);
			foreach ($by_line as $l=>$bl){
			if (($l == 0) and (strpos($bl, '#') === FALSE)){
				$benarkah = FALSE;
				break;
			}
			if (strpos($bl, '#') !== FALSE){
				if ($l == 0){
					$h = 0;
				}else{
					$h++;
				}
				if (isset($h)){
					$name = ltrim($bl, "#");
					$id = $this->m_hashtag->set_name($name)->find_id_by_name();
					if (empty($id)){
						$id = $this->m_hashtag->set_name($name)->insert();
					}
					$hash[$h]['name'] = $name;
					$hash[$h]['id'] = $id;
				}else{
					$benarkah = FALSE;
					break;
				}
			}else{
				if (isset($h)){
					if (strlen($bl)>0){
						$hash[$h]['items'][] = $bl;
					}
				}else{
					$benarkah = FALSE;
					break;
				}
			}
		}
		
		if ($benarkah){
			$user_detail = $this->authentication->user_detail();
			$log = array('user_id'=>$user_detail['id'], 'log_date'=>$date, 'last_modified'=>$date);
			$items = array();
			foreach($hash as $ha){
				$hashtag_id = $ha['id'];
				if (!isset($ha['items'])){
					$benarkah = FALSE;
					break;
				}
				foreach ($ha['items'] as $hi){
					$items[] = array('hashtag_id'=>$hashtag_id, 'hashtag'=>$ha['name'],'item'=>$hi);
				}
			}
			if ($benarkah)
				$hasil = $this->m_logs->insert_trans_log($log, $items);
		}
	}
	
	private function get_all_log(){
		$this->load->model(array('m_logs'));
		$log_item = array();
		$user = $this->input->get('user') ? $this->input->get('user') : null;
		$next = $this->input->get('page') ? $this->input->get('page') : 0;
		
		$logs = $this->m_logs->set_user_id($user)->get_all($next);

		foreach ($logs as $l){
			$log_item[] = $l->id;
		}
		$composite = $this->m_logs->get_composite($log_item);
		$log['log_id'] = $log['log_date'] = $log['last_modified'] = $log['score'] = $hash['hashtag_id'] = $hash['hashtag_name'] = $item['log_item_id'] = $item['item'] = $item['status'] = $comment['comment_time'] = $comment['comment'] = $comment['user_id'] = $comment['username'] = $comment['email'] = '';
		
		$jumlah = count($logs);
		
		$data = $klub_hash = $klub_item = $klub_comment = array();
		foreach ($composite as $i=>$c){
		
			if($i == 0){
				$i_log = $i_hash = $i_item = $i_comment = 0;
			}elseif($data[$i_log]['log_id'] != $c->log_id){
				$i_log++;
				$i_hash = $i_item = $i_comment = 0;
			}elseif($data[$i_log]['hash'][$i_hash]['hashtag_id'] != $c->hashtag_id){
				$i_hash++;
				$i_item = $i_comment = 0;
			}elseif($data[$i_log]['hash'][$i_hash]['items'][$i_item]['log_item_id'] != $c->log_item_id){
				$i_item++;
				$i_comment = 0;
			}
			$data[$i_log]['log_id'] = $c->log_id;
			$data[$i_log]['log_id'] = $c->log_id;
			$data[$i_log]['log_date'] = $c->log_date;
			$data[$i_log]['last_modified'] = $c->last_modified;
			$data[$i_log]['score'] = $c->score;
			$data[$i_log]['user_id'] = $c->log_user_id;
			$data[$i_log]['username'] = $c->log_username;
			$data[$i_log]['email'] = $c->log_email;
			$data[$i_log]['hash'][$i_hash]['hashtag_id'] = $c->hashtag_id;
			$data[$i_log]['hash'][$i_hash]['hashtag_name'] = $c->hashtag_name;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['log_item_id'] = $c->log_item_id;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['item'] = $c->item;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['status'] = $c->status;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['comment_time'] = $c->comment_time;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['comment'] = $c->comment;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['user_id'] = $c->user_id;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['username'] = $c->username;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['email'] = $c->email;
			$i_comment++;
			
		}
		return $data;
	}
	
	function login(){
		$this->load->model(array('m_users'));
		$log = $this->session->userdata('logged_in');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if (empty($log) && $username){
		
			$result = $this->m_users->set_username($username)->set_password($password)->login();
			
			if ($result){
				$sess = array();
				foreach($result as $row){
					$sess = array(
						'id' => $row->id,
						'username' => $row->username,
						'email' => $row->email,
						'logged_id' => true
					);
				}
				$this->session->set_userdata('logged_in', $sess);
				redirect('home/login', 'refresh');
			}
		}elseif ($log){
			redirect('home');
		}
		
		$this->load->view('header');
		$this->load->view('home/v_home');
		$this->load->view('footer');
	}
	
	function logout(){
		$this->authentication->restricted();
		$this->authentication->log_out();
		redirect('home/login');
	}
}
	
?>