<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication {
	var $CI;
	
	function __construct(){
		$this->CI =& get_instance();
		
		$this->CI->load->library('session');
	}
	
	function is_signed_in(){
		return $this->CI->session->userdata('logged_in') ? TRUE : FALSE;
	}
	
	function restricted(){
		if (!$this->is_signed_in()) redirect('home/login', 'refresh');
	}
	
	function log_out(){
		$this->CI->session->unset_userdata('logged_in');
		$this->CI->session->sess_destroy();
	}
	
	function user_detail(){
		return $this->CI->session->userdata('logged_in');
	}

	function get_all(){
		$this->CI->load->model('m_users');
		return $this->CI->m_users->get_all();
	}

	function validate_user($id){
		$user = $this->CI->session->userdata('logged_in');
		return $id == $user['id'];
	}
}

?>