<?php
class Sensor_var extends CI_Controller {
	private $group_id, $state, $question, $id_sensor_var;
	function __construct(){
		parent::__construct();
		$this->authentication->restricted();
		$this->load->model(array('m_sensor_var'));
	}

	public function save(){
		$result = array('status'=>'error');
		$this->group_id = $this->input->post('group_id');
		$this->state = $this->input->post('state');
		$this->question = $this->input->post('question');
		$hasil = $this->m_sensor_var->set_id_sensor_group($this->group_id)->set_state($this->state)->set_question($this->question)->insert();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delete(){
		$result = array('status'=>'error');
		$this->id_sensor_var = $this->input->post('id');
		$hasil = $this->m_sensor_var->set_id_sensor_var($this->id_sensor_var)->delete();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get(){
		$result = array('status'=>'error');
		$this->group_id = $this->input->post('group_id');
		$this->state = $this->input->post('state');
		$hasil = $this->m_sensor_var->set_id_sensor_group($this->group_id)->set_state($this->state)->get_all();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = $hasil;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function edit(){
		$result = array('status'=>'error');
		$this->id_sensor_var = $this->input->post('id_sensor_var');
		$this->question = $this->input->post('question');
		$hasil = $this->m_sensor_var->set_id_sensor_var($this->id_sensor_var)->set_question($this->question)->update();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
?>