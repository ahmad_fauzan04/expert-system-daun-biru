<?php
class Sensor_recommendation extends CI_Controller {
	private $var_id, $recommendation, $id_sensor_recommendation;
	function __construct(){
		parent::__construct();
		$this->authentication->restricted();
		$this->load->model(array('m_sensor_recommendation'));
	}

	public function save(){
		$result = array('status'=>'error');
		$this->var_id = $this->input->post('var_id');
		$this->recommendation = $this->input->post('rec');
		$hasil = $this->m_sensor_recommendation->set_id_sensor_var($this->var_id)->set_recommendation($this->recommendation)->insert();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delete(){
		$result = array('status'=>'error');
		$this->id_sensor_recommendation = $this->input->post('id');
		$hasil = $this->m_sensor_recommendation->set_id_sensor_recommendation($this->id_sensor_recommendation)->delete();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function edit(){
		$result = array('status'=>'error');
		$this->id_sensor_recommendation = $this->input->post('id');
		$this->recommendation = $this->input->post('rec');
		$hasil = $this->m_sensor_recommendation->set_recommendation($this->recommendation)->set_id_sensor_recommendation($this->id_sensor_recommendation)->update();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get(){
		$result = array('status'=>'error');
		$this->equipment_id = $this->input->post('eq_id');
		$hasil = $this->m_sensor->set_id_equipment($this->equipment_id)->get_composite();
		//$hasil = $this->m_sensor_var->set_id_sensor_group($this->group_id)->set_state($this->state)->get_all();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = $hasil;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
?>