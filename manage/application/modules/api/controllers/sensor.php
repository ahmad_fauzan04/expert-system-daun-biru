<?php
class Sensor extends CI_Controller {
	private $equipment_id, $id_titik_ukur, $id_sensor_group;
	function __construct(){
		parent::__construct();
		$this->authentication->restricted();
		$this->load->model(array('m_sensor', 'm_sensor_group'));
	}

	public function get(){
		$result = array('status'=>'error');
		$this->equipment_id = $this->input->post('eq_id');
		$hasil = $this->m_sensor->set_id_equipment($this->equipment_id)->get_composite();
		//$hasil = $this->m_sensor_var->set_id_sensor_group($this->group_id)->set_state($this->state)->get_all();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = $hasil;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_group_list(){
		$result = array('status'=>'error');
		$this->equipment_id = $this->input->post('eq_id');
		$hasil = $this->m_sensor_group->set_id_equipment($this->equipment_id)->get_group_list();
		//$hasil = $this->m_sensor_var->set_id_sensor_group($this->group_id)->set_state($this->state)->get_all();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = $hasil;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_sensor_list(){
		$result = array('status'=>'error');
		$this->equipment_id = $this->input->post('eq_id');
		$hasil = $this->m_sensor->set_id_equipment($this->equipment_id)->get_sensor_list();
		//$hasil = $this->m_sensor_var->set_id_sensor_group($this->group_id)->set_state($this->state)->get_all();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = $hasil;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_question_list(){
		$result = array('status'=>'error');
		$this->equipment_id = $this->input->post('eq_id');
		$hasil = $this->m_sensor->set_id_equipment($this->equipment_id)->get_composite_question();
		//$hasil = $this->m_sensor_var->set_id_sensor_group($this->group_id)->set_state($this->state)->get_all();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = $hasil;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function save(){
		$result = array('status'=>'error');
		$this->id_titik_ukur = $this->input->post('id_tu');
		$this->id_sensor_group = $this->input->post('id_sg');
		$hasil = $this->m_sensor->set_id_titik_ukur($this->id_titik_ukur)->set_id_sensor_group($this->id_sensor_group)->insert();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delete(){
		$result = array('status'=>'error');
		$this->id_sensor = $this->input->post('id');
		$hasil = $this->m_sensor->set_id_sensor($this->id_sensor)->delete();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
?>