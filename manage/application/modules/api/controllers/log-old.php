<?php
class Log extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->authentication->restricted();
		$this->load->model(array('m_logs', 'm_log_items'));
	}

	public function add_comment(){
		$result = array('status'=>'error');
		$this->load->model(array('m_comments'));
		$log_item_id = $this->input->post('log_item_id');
		$user_detail = $this->authentication->user_detail();
		$user_id = $user_detail['id'];
		$text = $this->input->post('text');
		$hasil = $this->m_comments->set_log_item_id($log_item_id)->set_comment($text)->set_user_id($user_id)->insert();
		if ($hasil){
			$result['status'] = 'success';
			$result['data'] = $user_detail;
			$result['data']['comment_id'] = $hasil;
			$result['data']['log_item_id'] = $log_item_id;
			$result['data']['comment'] = $text;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_log(){
		$result = array('status'=>'error');
		$hasil = false;
		$this->load->model(array('m_hashtag'));
		$text = $this->input->post('addLog');
		$tmpDate = explode('/', $this->input->post('tanggal'));
		$date = date('Y-m-d', mktime(0, 0, 0, $tmpDate[1], $tmpDate[0], $tmpDate[2]));
		$benarkah = TRUE; //flag bisa lanjut apa engga
		
		$by_line = explode("\n", $text);
		if (count($by_line)>1){
			foreach ($by_line as $l=>$bl){
				if (($l == 0) and (strpos($bl, '#') === FALSE)){
					$benarkah = FALSE;
					break;
				}
				if (strpos($bl, '#') !== FALSE){
					if ($l == 0){
						$h = 0;
					}else{
						$h++;
					}
					if (isset($h)){
						$name = ltrim($bl, "#");
						$id = $this->m_hashtag->set_name($name)->find_id_by_name();
						if (empty($id)){
							$id = $this->m_hashtag->set_name($name)->insert();
						}
						$hash[$h]['name'] = $name;
						$hash[$h]['id'] = $id;
					}else{
						$benarkah = FALSE;
						break;
					}
				}else{
					if (isset($h)){
						if (strlen($bl)>0){
							$hash[$h]['items'][] = $bl;
						}
					}else{
						$benarkah = FALSE;
						break;
					}
				}
			}
			if ($benarkah){
				$user_detail = $this->authentication->user_detail();
				$log = array('user_id'=>$user_detail['id'], 'log_date'=>$date, 'last_modified'=>$date);
				$items = array();
				foreach($hash as $ha){
					$hashtag_id = $ha['id'];
					if (count($ha['items'])==0){
						$benarkah = FALSE;
						break;
					}
					foreach ($ha['items'] as $hi){
						$items[] = array('hashtag_id'=>$hashtag_id, 'hashtag'=>$ha['name'], 'item'=>$hi);
					}
				}
				if ($benarkah)
					$hasil = $this->m_logs->insert_trans_log2($log, $items);
			}
			if ($hasil > 0){
				$result['status'] = 'success';
				/*
				$result['data']['user_id'] = $user_detail['id'];
				$result['data']['username'] = $user_detail['username'];
				$result['data']['email'] = $user_detail['email'];
				$result['data']['image'] = md5($user_detail['email']);
				*/
				$result['data'] = $this->get_single_log($hasil);
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	private function get_single_log($log_id){
		$composite = $this->m_logs->get_composite($log_id);
		$log['log_id'] = $log['log_date'] = $log['last_modified'] = $log['score'] = $hash['hashtag_id'] = $hash['hashtag_name'] = $item['log_item_id'] = $item['item'] = $item['status'] = $comment['comment_time'] = $comment['comment'] = $comment['user_id'] = $comment['username'] = $comment['email'] = '';
		
		$data = $klub_hash = $klub_item = $klub_comment = array();
		foreach ($composite as $i=>$c){
		
			if($i == 0){
				$i_log = $i_hash = $i_item = $i_comment = 0;
			}elseif($data[$i_log]['log_id'] != $c->log_id){
				$i_log++;
				$i_hash = $i_item = $i_comment = 0;
			}elseif($data[$i_log]['hash'][$i_hash]['hashtag_id'] != $c->hashtag_id){
				$i_hash++;
				$i_item = $i_comment = 0;
			}elseif($data[$i_log]['hash'][$i_hash]['items'][$i_item]['log_item_id'] != $c->log_item_id){
				$i_item++;
				$i_comment = 0;
			}
			$data[$i_log]['log_id'] = $c->log_id;
			$data[$i_log]['log_id'] = $c->log_id;
			$data[$i_log]['log_date'] = $c->log_date;
			$data[$i_log]['last_modified'] = $c->last_modified;
			$data[$i_log]['score'] = $c->score;
			$data[$i_log]['user_id'] = $c->log_user_id;
			$data[$i_log]['username'] = $c->log_username;
			$data[$i_log]['email'] = $c->log_email;
			$data[$i_log]['hash'][$i_hash]['hashtag_id'] = $c->hashtag_id;
			$data[$i_log]['hash'][$i_hash]['hashtag_name'] = $c->hashtag_name;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['log_item_id'] = $c->log_item_id;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['item'] = $c->item;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['status'] = $c->status;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['comment_time'] = $c->comment_time;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['comment'] = $c->comment;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['user_id'] = $c->user_id;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['username'] = $c->username;
			$data[$i_log]['hash'][$i_hash]['items'][$i_item]['comments'][$i_comment]['email'] = $c->email;
			$i_comment++;
			
		}
		return $data;
	}
	
	public function change_item_status(){
		$result = array('status'=>'error');
		$id = $this->input->post('item_id');
		$status = $this->input->post('status');
		$log_id = $this->m_log_items->set_id($id)->get_log_id();
		$user_id = $this->m_logs->set_id($log_id)->get_user_id();
		if ($this->authentication->validate_user($user_id)){
			$hasil = $this->m_log_items->set_id($id)->set_status($status)->update_item_status();
			$this->m_logs->set_id($log_id)->update_modified_time();
		}else{
			$hasil = FALSE;
		}
		if ($hasil){
			$result['status'] = 'success';
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_item(){
		$result = array('status'=>'error');
		$id = $this->input->post('item_id');
		$log_id = $this->m_log_items->set_id($id)->get_log_id();
		$user_id = $this->m_logs->set_id($log_id)->get_user_id();
		if ($this->authentication->validate_user($user_id)){
			$hasil = $this->m_log_items->set_id($id)->delete_log_item();
			$this->m_logs->set_id($log_id)->update_modified_time();
		}else{
			$hasil = FALSE;
		}
		if ($hasil){
			$result['status'] = 'success';
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
?>