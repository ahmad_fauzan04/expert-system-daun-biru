<?php
class Sensor_group extends CI_Controller {
	private $eq_id, $name, $id_sensor_group, $high_limit, $low_limit;
	function __construct(){
		parent::__construct();
		$this->authentication->restricted();
		$this->load->model(array('m_sensor_group'));
	}

	public function get(){
		$result = array('status'=>'error');
		$this->eq_id = $this->input->post('eq_id');
		$hasil = $this->m_sensor_group->set_id_equipment($this->eq_id)->get_all();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = $hasil;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function save(){
		$result = array('status'=>'error');
		$this->eq_id = $this->input->post('eq_id');
		$this->name = $this->input->post('group');
		$this->high_limit = $this->input->post('high_limit');
		$this->low_limit = $this->input->post('low_limit');
		$hasil = $this->m_sensor_group->set_id_equipment($this->eq_id)->set_name($this->name)->set_high_limit($this->high_limit)->set_low_limit($this->low_limit)->insert();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function edit(){
		$result = array('status' => 'error');
		$this->id_sensor_group = $this->input->post('id');
		$this->name = $this->input->post('name');
		$this->high_limit = $this->input->post('high_limit');
		$this->low_limit = $this->input->post('low_limit');
		
		$m_sensor = $this->m_sensor_group;
		if (!empty($this->name)){
			$m_sensor=$m_sensor->set_name($this->name);
		}
		if (!empty($this->high_limit)){
			$m_sensor=$m_sensor->set_high_limit($this->high_limit);
		}
		if (!empty($this->low_limit)){
			$m_sensor=$m_sensor->set_low_limit($this->low_limit);
		}
		
		$hasil = $m_sensor->set_id_sensor_group($this->id_sensor_group)->update();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delete(){
		$result = array('status'=>'error');
		$this->id_sensor_group = $this->input->post('id');
		$hasil = $this->m_sensor_group->set_id_sensor_group($this->id_sensor_group)->delete();
		if (isset($hasil)){
			$result['status'] = 'success';
			$result['data'] = array();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_related(){
		$result = array('status'=>'error');
		$this->eq_id = $this->input->get('eq_id');
		$hasil = $this->m_sensor_group->get_related();
		if (isset($hasil)){
			$ar = array();
			foreach ($hasil as $l){
				$ar[$l->id_sensor_group]['name'] = $l->name;
				$ar[$l->id_sensor_group]['high_limit'] = $l->high_limit;
				$ar[$l->id_sensor_group]['low_limit'] = $l->low_limit;
				$ar[$l->id_sensor_group][$l->state][$l->id_sensor_var]['question'] = $l->question;
				$ar[$l->id_sensor_group][$l->state][$l->id_sensor_var]['rec'][$l->id_sensor_recommendation] = $l->recommendation;
			}
			$result['status'] = 'success';
			$result['data'] = $ar;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
?>