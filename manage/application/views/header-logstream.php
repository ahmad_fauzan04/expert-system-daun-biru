<html>
<head>
	<title>Daily Log</title>
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/fui/css/flat-ui.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/datepicker.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/date.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/autosize.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/flatui-checkbox.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/flatui-radio.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery.tagsinput.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/bootstrap-typeahead.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/fui/js/application.js"></script>
	<script>
	var base_url = "<?=base_url(); ?>";
	$(document).ready(function(){
		$('#user').click(function(){
			$('.userfilter').slideToggle(500, 'swing');
		}); 
		$(function() {
			$('#datepicker').datepicker({format: "dd/mm/yy"});
		});
		$(function(){
			$('#datepicker2').datepicker({format: "dd/mm/yy"}).val(new Date().asString()).trigger('change');
		});
		$('.media-list').on("click", ".media-border", function(){
			id = $(this).attr('data-id');
			$('.ch-'+id).toggle('fast');
			$('.comment').hide('fast');
		});
		
		$(".media-list").on({
			mouseenter: function(){
				id = $(this).attr('value');
				$('.bc'+id).css('display', 'inline-block');
				$('.di'+id).css('display', 'inline-block');
			},
			mouseleave: function(){
				$('.bc'+id).css('display', 'none');
				$('.di'+id).css('display', 'none');
			}
		}, '.item-hashtag');
		$(".comment").click(function(e){
			e.stopPropagation();
			e.preventDefault();
		});
		$('.media-body').on('click', '.delete-item',function(e){
			e.stopPropagation();
			e.preventDefault();
			id = $(this).attr('value');
			delete_item(id);
		});
		$('.comment-submit').click(function(e){
			id = $(this).attr('data-id');
			text = $('textarea.tc-'+id).val()
			$('.text-'+id).val(text);
			$('.form-'+id).submit();
		});
		
		$('.bubble-chat').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			id = $(this).attr('value');
			$('.comment').hide('fast');
			$('.cm-'+id).toggle('fast');
		});
		
		$(".item-hashtag").click(function(e){
			e.stopPropagation();
			e.preventDefault();
			/*
			id = $(this).attr('value');
			$('.comment').hide('fast');
			$('.cm-'+id).toggle('fast');
			*/
		});
		$(".item-icon").click(function(e){
			e.stopPropagation();
			e.preventDefault();
			var id = $(this).attr('data-id');
			var status = $(this).attr('data-status');
			if (status == 0){
				status1 = 1;
			}else{
				status1 = 0;
			}
			update_status(id, status1, this);
			
		});
		/*
		$(".submit-status").click(function(e){
			e.preventDefault();
			var addlog = $('.addlog').val();
			var tanggal = $('.textbox-datepicker').val();
			submit_log(addlog, tanggal);
		});
*/
		$(".form-update-log").submit(function(e){
			e.preventDefault();
			var addlog = $(this).find('#add').val();
			var tanggal = $(this).find('#datepicker2').val();
			submit_log(addlog, tanggal);
		});
		$( ".update-log textarea" ).click(function() {
		  $(this).animate({height: "80px"}, 200, function(){
			$(this).delay(1000).css("min-height", "10px");
		  });
		});

		$('textarea').autosize();

		$('.form-update-log').keydown(function(e) {
			if(e.keyCode == 13 && e.ctrlKey){
				$('#add').trigger('submit');
			}
		});

		$(".form-comment").submit(function(e){
			e.preventDefault();
			var id = $(this).find('[name=log_item_id]').val();
			var text = $(this).find('.textarea-comment').val();
			submit_comment(id,text);
		});
	});	
	
	function update_status(id, status, elem){
		$.ajax({
			url: base_url+'api/log/change_item_status',
			dataType: 'json',
			data: {"item_id":id, "status":status},
			method: 'POST',
			success: function(resp){
				if (resp.status == 'success'){
					if (status == 1){
						$(elem).attr('data-status', 1);
						$(elem).removeClass('glyphicon-minus');
						$(elem).addClass('glyphicon-ok');
					}else{
						$(elem).attr('data-status', 0);
						$(elem).removeClass('glyphicon-ok');
						$(elem).addClass('glyphicon-minus');
					}
				}
			}
		});
	}

	function delete_item(id){
		$.ajax({
			url: base_url+'api/log/delete_item',
			dataType: 'json',
			data: {"item_id":id},
			method: 'POST',
			success: function(resp){
				if (resp.status == 'success'){
					$('.cm-'+id).remove();
					$('.il-'+id).remove();
				}
			}
		});
	}

	function submit_comment(id,text){
		$.ajax({
			url:base_url+'api/log/add_comment',
			dataType:'json',
			data: {"log_item_id":id, "text":text},
			method: 'POST',
			success: function(resp){
				$.each(resp.data, function(){
					strHtml ='<li class="media-comment">';
						strHtml +='<div class="pull-left">';
							strHtml +='<div class="media-object name-commenter" data-id="'+resp.data.id+'">';
								strHtml +='<strong>'+resp.data.username+':</strong>';
							strHtml +='</div>';
						strHtml +='</div>';
						strHtml +='<div class="media-body">';
							strHtml +='<div class="content-comment" name="text" data-id="'+resp.data.comment_id+'">'+resp.data.comment+'</div>';
						strHtml +='</div>';
					strHtml +='</li>';
				});
				$(".textarea-comment").val("");
				$(".comment-list").append(strHtml);
			}
		});
	}

	function submit_log(addlog, tanggal){
		$.ajax({
			url:base_url+'api/log/add_log',
			dataType: 'json',
			data: {"addLog":addlog, "tanggal":tanggal},
			method:'POST',
			success: function(resp){
				$.each(resp.data, function(i, item){
					strHtml = '<div class="media media-border" data-id="'+resp.data[i].log_id+'">';
						strHtml += '<a class="pull-left">';
							strHtml += '<img class="media-object photo-log" src="http://www.gravatar.com/avatar/'+resp.data[i].image+'?d=http%3A%2F%2Fwww0.artflakes.com%2Fsystem%2Fprofiles%2Ficons%2F50217%2Fsmall_icon.jpg%3F1387055137">';
						strHtml += '</a>';
						strHtml += '<div class="media-body media-log">';
							strHtml += '<div class="content-name" data-id="'+resp.data[i].user_id+'">';
								strHtml += '<a href="#">'+resp.data[i].username+'</a>';
							strHtml += '</div>';

					$.each(resp.data[i].hash, function(j, hash){
						
							strHtml += '<div class="hashtag" data-id="'+resp.data[i].hash[j].hashtag_id+'">#'+resp.data[i].hash[j].hashtag_name+'</div>';
							strHtml += '<div class="content-hashtag ch-'+resp.data[i].log_id+'">';
						
						$.each(resp.data[i].hash[j].items, function(k, items){

								strHtml += '<div class="pending item-list il-'+resp.data[i].hash[j].items[k].log_item_id+'">';
									strHtml += '<div class="media-list">';
										strHtml += '<div class="media">';
											strHtml += '<span class="pull-left">';
												strHtml += '<div class="item-icon media-object glyphicon glyphicon-minus" data-id='+resp.data[i].hash[j].items[k].log_item_id+' data-status='+resp.data[i].hash[j].items[k].status+'></div>';
											strHtml += '</span>';
											strHtml += '<div class="media-body item-hashtag" value="'+resp.data[i].hash[j].items[k].log_item_id+'">'+resp.data[i].hash[j].items[k].item+'';
												strHtml += '<span class="bubble-chat glyphicon glyphicon-comment pull-right bc'+resp.data[i].hash[j].items[k].log_item_id+'" value='+resp.data[i].hash[j].items[k].log_item_id+'></span>&nbsp;';
												strHtml += '<span class="delete-item glyphicon glyphicon-remove pull-right di'+resp.data[i].hash[j].items[k].log_item_id+'" value='+resp.data[i].hash[j].items[k].log_item_id+'></span>';
											strHtml += '</div>';
										strHtml += '</div>';
									strHtml += '</div>';
								strHtml += '</div>';

								strHtml +='<div class="comment cm-'+resp.data[i].hash[j].items[k].log_item_id+'">';
									strHtml +='<div class="comment-area">';
										strHtml +='<ul class="media-list comment-list">';


									$.each(resp.data[i].hash[j].items[k].comments, function(l, comments){ 
										if (resp.data[i].hash[j].items[k].comments, function(l, comments){

											strHtml +='<li class="media-comment">';
												strHtml +='<div class="pull-left">';
													strHtml +='<div class="media-object name-commenter" data-id="'+resp.data[i].hash[j].items[k].comments[l].user_id+'">';
														strHtml +='<strong>'+resp.data[i].hash[j].items[k].comments[l].username+':</strong>';
													strHtml +='</div>';
												strHtml +='</div>';
												strHtml +='<div class="media-body">';
													strHtml +='<div class="content-comment name="text"">'+resp.data[i].hash[j].items[k].comments[l].comment+'</div>';
												strHtml +='</div>';
											strHtml +='</li>';
										strHtml +='</ul>';
									}); });

										strHtml +='<form role="form" method="post" action="" class="form-'+resp.data[i].hash[j].items[k].log_item_id+' form-comment">';
											strHtml +='<div class="form-group">';
												strHtml +='<textarea name="text" placeholder="Leave a comment" rows="1" cols="50" class="form-control textarea-comment tc-'+resp.data[i].hash[j].items[k].log_item_id+'"></textarea>';
												strHtml +='<input type="hidden" name="text" class="text-'+resp.data[i].hash[j].items[k].log_item_id+'" value="">';
												strHtml +='<input type="hidden" name="log_item_id" value="'+resp.data[i].hash[j].items[k].log_item_id+'">';
												strHtml +='<input type="hidden" name="action" value="tambahKomentar">';
											strHtml +='</div>';
											strHtml +='<button type="submit" value="tambahKomentar" name="action" class="btn btn-sm btn-primary comment-submit" data-id="'+resp.data[i].hash[j].items[k].log_item_id+'">Post</button>';
										strHtml +='</form>';
									strHtml +='</div>';
								strHtml +='</div>';

						});
						
							strHtml += '</div>';
				   	});

							strHtml += '<div class="timestamp">';
								strHtml += '<div class="pull-left">';
									strHtml += '<p class="times">'+resp.data[i].log_date+'</p>';
								strHtml += '</div>';
							strHtml += '</div>';
						strHtml += '</div>';
					strHtml += '</div>';
				
					$('.media[data-id='+resp.data[i].log_id+']').hide();
					$('.media[data-id='+resp.data[i].log_id+']').remove();
				});

				$("#add").val("");
				$(".log-list").prepend(strHtml);
				$("#message").show().delay(5000).hide('slow');
				$(".bubble-chat").unbind();
				$(".bubble-chat").click(function(e){
					e.stopPropagation();
					e.preventDefault();
					id = $(this).attr('value');
					$('.comment').hide('fast');
					$('.cm-'+id).toggle('fast');
				});
				$(".comment").click(function(e){
					e.stopPropagation();
					e.preventDefault();
				});
				$('.comment-submit').click(function(e){
					id = $(this).attr('data-id');
					text = $('textarea.tc-'+id).val()
					$('.text-'+id).val(text);
					$('.form-'+id).submit();
				});
				$(".delete-item").click(function(e){
					e.stopPropagation();
					e.preventDefault();
					id = $(this).attr('value');
					delete_item(id);
				});
				$(".item-icon").click(function(e){
					e.stopPropagation();
					e.preventDefault();
					var id = $(this).attr('data-id');
					var status = $(this).attr('data-status');
					if (status == 0){
						status1 = 1;
					}else{
						status1 = 0;
					}
					update_status(id, status1, this);
				});
				$(".form-comment").submit(function(e){
					e.preventDefault();
					var id = $(this).find('[name=log_item_id]').val();
					var text = $(this).find('.textarea-comment').val();
					submit_comment(id,text);
				});
			}
		});
	}
	</script>
</head>
<body>
<div id="header">
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-collapse-03"></button>
	          	<a href="#" class="navbar-brand">Daily Log</a>
	        </div>
	        <div class="navbar-collapse collapse navbar-collapse-03">
	        	<ul class="nav navbar-nav navbar-right">
	        		<li class="divider"></li>
	        		<li class="dropdown">
	        			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		        			<span class="fui-alert"></span>
		        			<span class="hidden-lg">Notification</span>
		        			<span class="navbar-new">1</span>
	        			</a>
	        			<ul class="dropdown-menu">
                      		<li><a href="#fakelink">Dita mengomentari log anda</a></li>
                      		<li><a href="#fakelink">Dita mengomentari log anda</a></li>
                    	</ul>
	        		</li>
	        		<li><a href="#"><img class="photo-prof" src="<?php echo'http://www.gravatar.com/avatar/'.md5(strtolower(trim($user['email']))).'?d=http%3A%2F%2Fwww0.artflakes.com%2Fsystem%2Fprofiles%2Ficons%2F50217%2Fsmall_icon.jpg%3F1387055137';?>"></a></li>
                  	<li class="dropdown">
                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      		Settings <b class="caret"></b>
                    	</a>
                    	<ul class="dropdown-menu">
                      		<li><a href="#fakelink">Settings</a></li>
                      		<li class="divider"></li>
                      		<li><a href="<?=base_url();?>home/logout">Logout</a></li>
                    	</ul>
                  	</li>
                </ul>
	        </div>
	    </div>
    </div>
</div>