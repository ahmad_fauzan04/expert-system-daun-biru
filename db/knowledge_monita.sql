-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for monita3
CREATE DATABASE IF NOT EXISTS `monita3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `monita3`;


-- Dumping structure for table monita3.daftar_alarm
CREATE TABLE IF NOT EXISTS `daftar_alarm` (
  `id_alarm` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(64) DEFAULT NULL,
  `waktu` varchar(16) DEFAULT NULL,
  `level_alarm` int(11) DEFAULT NULL,
  `accepted` int(10) unsigned DEFAULT NULL,
  `id_titik` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_alarm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.daftar_group
CREATE TABLE IF NOT EXISTS `daftar_group` (
  `id_jenis` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nama_group` char(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.data_harian
CREATE TABLE IF NOT EXISTS `data_harian` (
  `id_titik_ukur` int(11) DEFAULT NULL,
  `waktu` bigint(17) DEFAULT NULL,
  `data_tunggal` float DEFAULT NULL,
  `year` smallint(6) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `date` tinyint(4) NOT NULL,
  `hour` tinyint(4) NOT NULL,
  `minute` tinyint(4) NOT NULL,
  `second` tinyint(4) NOT NULL,
  KEY `id_titik_ukur` (`id_titik_ukur`,`waktu`),
  KEY `year` (`year`),
  KEY `month` (`month`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.data_harian_2012_05_22
CREATE TABLE IF NOT EXISTS `data_harian_2012_05_22` (
  `id_titik_ukur` int(11) DEFAULT NULL,
  `waktu` bigint(17) DEFAULT NULL,
  `data_tunggal` float DEFAULT NULL,
  `hour` tinyint(4) NOT NULL,
  `minute` tinyint(4) NOT NULL,
  `second` tinyint(4) NOT NULL,
  KEY `id_titik_ukur` (`id_titik_ukur`,`waktu`),
  KEY `hour` (`hour`),
  KEY `minute` (`minute`),
  KEY `second` (`second`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.data_harian_dummy
CREATE TABLE IF NOT EXISTS `data_harian_dummy` (
  `id_titik_ukur` int(11) DEFAULT NULL,
  `waktu` bigint(17) DEFAULT NULL,
  `data_tunggal` float DEFAULT NULL,
  `hour` tinyint(4) NOT NULL,
  `minute` tinyint(4) NOT NULL,
  `second` tinyint(4) NOT NULL,
  KEY `id_titik_ukur` (`id_titik_ukur`,`waktu`),
  KEY `hour` (`hour`),
  KEY `minute` (`minute`),
  KEY `second` (`second`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.data_jaman
CREATE TABLE IF NOT EXISTS `data_jaman` (
  `id_titik_ukur` int(11) DEFAULT NULL,
  `waktu` bigint(17) DEFAULT NULL,
  `data_tunggal` float DEFAULT NULL,
  `year` smallint(6) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `date` tinyint(4) NOT NULL,
  `hour` tinyint(4) NOT NULL,
  `minute` tinyint(4) NOT NULL,
  `second` tinyint(4) NOT NULL,
  KEY `id_titik_ukur` (`id_titik_ukur`,`waktu`),
  KEY `year` (`year`),
  KEY `month` (`month`),
  KEY `date` (`date`),
  KEY `hour` (`hour`),
  KEY `minute` (`minute`),
  KEY `second` (`second`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.data_tunggal
CREATE TABLE IF NOT EXISTS `data_tunggal` (
  `id_data` int(11) NOT NULL AUTO_INCREMENT,
  `id_titik_ukur` int(11) DEFAULT NULL,
  `waktu` varchar(20) DEFAULT NULL,
  `id_waktu` int(10) unsigned NOT NULL,
  `data_tunggal` float DEFAULT NULL,
  `data_maks` float DEFAULT NULL,
  `data_min` float DEFAULT NULL,
  `year` smallint(6) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `date` tinyint(4) NOT NULL,
  `hour` tinytext NOT NULL,
  `minute` tinyint(4) NOT NULL,
  `second` tinyint(4) NOT NULL,
  `milisecond` smallint(6) NOT NULL,
  PRIMARY KEY (`id_data`),
  KEY `year` (`year`),
  KEY `month` (`month`),
  KEY `date` (`date`),
  KEY `id_titik_ukur` (`id_titik_ukur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.departemen
CREATE TABLE IF NOT EXISTS `departemen` (
  `id_dep` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dep` varchar(20) DEFAULT NULL,
  `id_pers` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.dynamic_range
CREATE TABLE IF NOT EXISTS `dynamic_range` (
  `titik_ukur` int(11) DEFAULT NULL,
  `presentase` int(11) DEFAULT NULL,
  `low` int(11) DEFAULT NULL,
  `low_low` int(11) DEFAULT NULL,
  `high` int(11) DEFAULT NULL,
  `high_high` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.equipment
CREATE TABLE IF NOT EXISTS `equipment` (
  `id_equipment` int(11) NOT NULL AUTO_INCREMENT,
  `id_dept` int(11) DEFAULT NULL,
  `nama_equipment` varchar(40) DEFAULT NULL,
  `jenis_equipment` int(11) DEFAULT NULL,
  `ket_equipment` varchar(128) DEFAULT NULL,
  `kode_equipment` varchar(6) DEFAULT NULL,
  `tahun_pembuatan` int(6) DEFAULT NULL,
  `merek` varchar(50) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `kapasitas_terpasang` int(11) DEFAULT NULL,
  `kapasitas_normal_min` int(11) DEFAULT NULL,
  `kapasitas_normal_max` int(11) DEFAULT NULL,
  `key_parameter` varchar(50) DEFAULT NULL,
  `s.f.c.` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_equipment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.group_titik_ukur
CREATE TABLE IF NOT EXISTS `group_titik_ukur` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `id_dept` int(11) DEFAULT NULL,
  `id_persh` int(11) DEFAULT NULL,
  `id_equipment` int(11) DEFAULT NULL,
  `nama_group` varchar(20) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `gambar` varchar(45) DEFAULT NULL,
  `jenis_group` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.jowm
CREATE TABLE IF NOT EXISTS `jowm` (
  `id_data` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `data` double DEFAULT NULL,
  PRIMARY KEY (`id_data`),
  KEY `id_data` (`id_data`,`waktu`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1 COMMENT='tabel row dari scada';

-- Data exporting was unselected.


-- Dumping structure for table monita3.knowledge_equipment
CREATE TABLE IF NOT EXISTS `knowledge_equipment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.knowledge_indikasi
CREATE TABLE IF NOT EXISTS `knowledge_indikasi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_knowledge_sensorgroup` int(11) NOT NULL,
  `indikasi` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_knowledge_indikasi_knowledge_sensorgroup` (`id_knowledge_sensorgroup`),
  CONSTRAINT `FK_knowledge_indikasi_knowledge_sensorgroup` FOREIGN KEY (`id_knowledge_sensorgroup`) REFERENCES `knowledge_sensorgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.knowledge_rekomendasi
CREATE TABLE IF NOT EXISTS `knowledge_rekomendasi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_indikasi` int(11) unsigned NOT NULL,
  `isi_rekomendasi` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_knowledge_rekomendasi_knowledge_indikasi` (`id_indikasi`),
  CONSTRAINT `FK_knowledge_rekomendasi_knowledge_indikasi` FOREIGN KEY (`id_indikasi`) REFERENCES `knowledge_indikasi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.knowledge_sensor
CREATE TABLE IF NOT EXISTS `knowledge_sensor` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_titik_ukur` int(11) NOT NULL DEFAULT '0',
  `id_knowledge_sensorgroup` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_knowledge_sensor_titik_ukur` (`id_titik_ukur`),
  KEY `FK_knowledge_sensor_knowledge_sensorgroup` (`id_knowledge_sensorgroup`),
  CONSTRAINT `FK_knowledge_sensor_knowledge_sensorgroup` FOREIGN KEY (`id_knowledge_sensorgroup`) REFERENCES `knowledge_sensorgroup` (`id`),
  CONSTRAINT `FK_knowledge_sensor_titik_ukur` FOREIGN KEY (`id_titik_ukur`) REFERENCES `titik_ukur` (`id_titik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.knowledge_sensorgroup
CREATE TABLE IF NOT EXISTS `knowledge_sensorgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_knowledge_equipment` int(11) unsigned NOT NULL DEFAULT '0',
  `nama` varchar(50) NOT NULL DEFAULT '0',
  `high` int(11) NOT NULL DEFAULT '0',
  `low` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_knowledge_sensorgroup_knowledge_equipment` (`id_knowledge_equipment`),
  CONSTRAINT `FK_knowledge_sensorgroup_knowledge_equipment` FOREIGN KEY (`id_knowledge_equipment`) REFERENCES `knowledge_equipment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.maintenance_log
CREATE TABLE IF NOT EXISTS `maintenance_log` (
  `tanggal` timestamp NULL DEFAULT NULL,
  `kegiatan` varchar(50) DEFAULT NULL,
  `id_equipment` int(11) DEFAULT NULL,
  `id_titik_ukur` int(11) DEFAULT NULL,
  `id_part` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.part_equipment
CREATE TABLE IF NOT EXISTS `part_equipment` (
  `id_part` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.perusahaan
CREATE TABLE IF NOT EXISTS `perusahaan` (
  `id_pers` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pers` varchar(30) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `gambar` blob,
  PRIMARY KEY (`id_pers`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.relasi
CREATE TABLE IF NOT EXISTS `relasi` (
  `id_relasi` int(11) DEFAULT NULL,
  `tipe_relasi` varchar(50) DEFAULT NULL,
  `range_min` int(11) DEFAULT NULL,
  `range_max` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.relasi_titik_ukur
CREATE TABLE IF NOT EXISTS `relasi_titik_ukur` (
  `id_titik_ukur1` int(11) DEFAULT NULL,
  `id_titik_ukur2` int(11) DEFAULT NULL,
  `id_relasi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.tipe_data
CREATE TABLE IF NOT EXISTS `tipe_data` (
  `id_tipe` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nama_tipe` varchar(50) NOT NULL,
  `kode` varchar(6) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_tipe`),
  UNIQUE KEY `nama_tipe` (`nama_tipe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table monita3.titik_ukur
CREATE TABLE IF NOT EXISTS `titik_ukur` (
  `id_titik` int(11) NOT NULL AUTO_INCREMENT,
  `id_group_titik_ukur` int(11) DEFAULT NULL,
  `id_equipment` int(11) DEFAULT NULL,
  `id_titik_global` int(11) DEFAULT NULL,
  `kode_titik` varchar(10) DEFAULT NULL,
  `nama_titik` varchar(64) DEFAULT NULL,
  `serial_no` int(11) NOT NULL,
  `no_port` int(11) DEFAULT NULL,
  `no_kanal` int(11) DEFAULT NULL,
  `status` char(20) DEFAULT NULL,
  `tipe_sensor` int(11) DEFAULT NULL,
  `tipe_sinyal` int(11) DEFAULT NULL,
  `tipe_data` int(11) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `fasa` float DEFAULT NULL,
  `mask` smallint(6) NOT NULL,
  `range_min` float DEFAULT '0',
  `range_max` float DEFAULT '9999',
  `alarm_low1` float DEFAULT '0',
  `alarm_low2` float DEFAULT '0',
  `alarm_high1` float DEFAULT '9999',
  `alarm_high2` float DEFAULT '9999',
  `keterangan` varchar(20) DEFAULT NULL,
  `sumber_data` int(10) unsigned DEFAULT NULL,
  `id_titik_komb1` int(11) DEFAULT '0',
  `id_titik_komb2` int(11) DEFAULT '0',
  `operasi` varchar(5) DEFAULT '0',
  `alarm_setting` int(11) NOT NULL DEFAULT '0',
  `kalibrasi_a` double NOT NULL DEFAULT '1',
  `kalibrasi_b` double NOT NULL DEFAULT '0',
  `dynamic_range_enabled` int(11) NOT NULL DEFAULT '0',
  `id_relasi` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_titik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
