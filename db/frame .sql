-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.32 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for monita3
CREATE DATABASE IF NOT EXISTS `monita3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `monita3`;


-- Dumping structure for table monita3.sensor
CREATE TABLE IF NOT EXISTS `sensor` (
  `id_sensor` int(11) NOT NULL AUTO_INCREMENT,
  `id_titik_ukur` int(11) NOT NULL DEFAULT '0',
  `id_sensor_group` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_sensor`),
  UNIQUE KEY `id_titik_ukur_id_sensor_group` (`id_titik_ukur`,`id_sensor_group`),
  KEY `FK__sensor_group` (`id_sensor_group`),
  CONSTRAINT `FK__sensor_group` FOREIGN KEY (`id_sensor_group`) REFERENCES `sensor_group` (`id_sensor_group`),
  CONSTRAINT `FK__titik_ukur` FOREIGN KEY (`id_titik_ukur`) REFERENCES `titik_ukur` (`id_titik`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='relating sensor_group with titik_ukur';

-- Dumping data for table monita3.sensor: ~9 rows (approximately)
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
INSERT INTO `sensor` (`id_sensor`, `id_titik_ukur`, `id_sensor_group`, `name`) VALUES
	(1, 12092, 1, 'a'),
	(2, 12093, 1, 'b'),
	(3, 12094, 1, 'c'),
	(4, 12095, 1, 'd'),
	(5, 12096, 1, 'e'),
	(6, 12097, 1, 'f'),
	(7, 12098, 1, 'g'),
	(8, 12099, 1, 'h'),
	(9, 12100, 1, 'i');
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;


-- Dumping structure for table monita3.sensor_group
CREATE TABLE IF NOT EXISTS `sensor_group` (
  `id_sensor_group` int(11) NOT NULL AUTO_INCREMENT,
  `id_equipment` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `high_limit` decimal(10,0) NOT NULL DEFAULT '0',
  `low_limit` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_sensor_group`),
  KEY `FK__equipment` (`id_equipment`),
  CONSTRAINT `FK__equipment` FOREIGN KEY (`id_equipment`) REFERENCES `equipment` (`id_equipment`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='grouping sensor for group rules purpose';

-- Dumping data for table monita3.sensor_group: ~1 rows (approximately)
/*!40000 ALTER TABLE `sensor_group` DISABLE KEYS */;
INSERT INTO `sensor_group` (`id_sensor_group`, `id_equipment`, `name`, `high_limit`, `low_limit`) VALUES
	(1, 2, 'exhaust', 490, 200);
/*!40000 ALTER TABLE `sensor_group` ENABLE KEYS */;


-- Dumping structure for table monita3.sensor_recommendation
CREATE TABLE IF NOT EXISTS `sensor_recommendation` (
  `id_sensor_recommendation` int(11) NOT NULL AUTO_INCREMENT,
  `id_sensor_var` int(11) NOT NULL DEFAULT '0',
  `recommendation` varchar(500) NOT NULL,
  `cost` tinyint(4) NOT NULL DEFAULT '0',
  `resource` tinyint(4) NOT NULL DEFAULT '0',
  `time_needed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_sensor_recommendation`),
  KEY `FK__sensor_var` (`id_sensor_var`),
  CONSTRAINT `FK__sensor_var` FOREIGN KEY (`id_sensor_var`) REFERENCES `sensor_var` (`id_sensor_var`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='keep recommendation after inference';

-- Dumping data for table monita3.sensor_recommendation: ~3 rows (approximately)
/*!40000 ALTER TABLE `sensor_recommendation` DISABLE KEYS */;
INSERT INTO `sensor_recommendation` (`id_sensor_recommendation`, `id_sensor_var`, `recommendation`, `cost`, `resource`, `time_needed`) VALUES
	(1, 1, 'Periksa sistem saluran gas buang yang bocor & perbaiki jika perlu', 0, 0, 0),
	(2, 2, 'Periksa saluran gas buang atau udara masuk. Bersihkan jika perlu', 0, 0, 0),
	(3, 3, 'Periksa, ganti jika perlu, indikator (sensor)', 0, 0, 0);
/*!40000 ALTER TABLE `sensor_recommendation` ENABLE KEYS */;


-- Dumping structure for table monita3.sensor_var
CREATE TABLE IF NOT EXISTS `sensor_var` (
  `id_sensor_var` int(11) NOT NULL AUTO_INCREMENT,
  `id_sensor_group` int(11) NOT NULL DEFAULT '0',
  `state` enum('sensor_high','sensor_low','group_high','group_low') NOT NULL COMMENT 'which state this variable apply',
  `question` varchar(300) NOT NULL,
  `value` bit(1) NOT NULL COMMENT '0=false, 1=true',
  `last_check` datetime NOT NULL,
  PRIMARY KEY (`id_sensor_var`),
  KEY `FK_sv_sensor_group` (`id_sensor_group`),
  CONSTRAINT `FK_sv_sensor_group` FOREIGN KEY (`id_sensor_group`) REFERENCES `sensor_group` (`id_sensor_group`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='additional variable need to be checked before inference';

-- Dumping data for table monita3.sensor_var: ~3 rows (approximately)
/*!40000 ALTER TABLE `sensor_var` DISABLE KEYS */;
INSERT INTO `sensor_var` (`id_sensor_var`, `id_sensor_group`, `state`, `question`, `value`, `last_check`) VALUES
	(1, 1, 'group_high', 'Saluran gas buang bocor?', b'0', '2010-00-00 00:00:00'),
	(2, 1, 'group_high', 'Saluran gas buang atau udara masuk tersumbat?', b'0', '0000-00-00 00:00:00'),
	(3, 1, 'sensor_high', 'Setelan timing katup kurang sesuai?', b'0', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sensor_var` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
