-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table monita3.sensor
CREATE TABLE IF NOT EXISTS `sensor` (
  `id_sensor` int(11) NOT NULL AUTO_INCREMENT,
  `id_titik_ukur` int(11) NOT NULL DEFAULT '0',
  `id_sensor_group` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_sensor`),
  UNIQUE KEY `id_titik_ukur_id_sensor_group` (`id_titik_ukur`,`id_sensor_group`),
  KEY `FK__sensor_group` (`id_sensor_group`),
  CONSTRAINT `FK__sensor_group` FOREIGN KEY (`id_sensor_group`) REFERENCES `sensor_group` (`id_sensor_group`),
  CONSTRAINT `FK__titik_ukur` FOREIGN KEY (`id_titik_ukur`) REFERENCES `titik_ukur` (`id_titik`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='relating sensor_group with titik_ukur';

-- Dumping data for table monita3.sensor: ~8 rows (approximately)
DELETE FROM `sensor`;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
INSERT INTO `sensor` (`id_sensor`, `id_titik_ukur`, `id_sensor_group`) VALUES
	(1, 12092, 1),
	(2, 12093, 1),
	(3, 12094, 1),
	(5, 12096, 1),
	(6, 12097, 1),
	(7, 12098, 1),
	(8, 12099, 1),
	(9, 12100, 1);
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;


-- Dumping structure for table monita3.sensor_group
CREATE TABLE IF NOT EXISTS `sensor_group` (
  `id_sensor_group` int(11) NOT NULL AUTO_INCREMENT,
  `id_equipment` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `high_limit` decimal(10,0) NOT NULL DEFAULT '0',
  `low_limit` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_sensor_group`),
  KEY `FK__equipment` (`id_equipment`),
  CONSTRAINT `FK__equipment` FOREIGN KEY (`id_equipment`) REFERENCES `equipment` (`id_equipment`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='grouping sensor for group rules purpose';

-- Dumping data for table monita3.sensor_group: ~1 rows (approximately)
DELETE FROM `sensor_group`;
/*!40000 ALTER TABLE `sensor_group` DISABLE KEYS */;
INSERT INTO `sensor_group` (`id_sensor_group`, `id_equipment`, `name`, `high_limit`, `low_limit`) VALUES
	(1, 2, 'exhaust', 490, 200);
/*!40000 ALTER TABLE `sensor_group` ENABLE KEYS */;


-- Dumping structure for table monita3.sensor_recommendation
CREATE TABLE IF NOT EXISTS `sensor_recommendation` (
  `id_sensor_recommendation` int(11) NOT NULL AUTO_INCREMENT,
  `id_sensor_var` int(11) NOT NULL DEFAULT '0',
  `recommendation` varchar(500) NOT NULL,
  `cost` tinyint(4) NOT NULL DEFAULT '0',
  `resource` tinyint(4) NOT NULL DEFAULT '0',
  `time_needed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_sensor_recommendation`),
  KEY `FK__sensor_var` (`id_sensor_var`),
  CONSTRAINT `FK__sensor_var` FOREIGN KEY (`id_sensor_var`) REFERENCES `sensor_var` (`id_sensor_var`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='keep recommendation after inference';

-- Dumping data for table monita3.sensor_recommendation: ~3 rows (approximately)
DELETE FROM `sensor_recommendation`;
/*!40000 ALTER TABLE `sensor_recommendation` DISABLE KEYS */;
INSERT INTO `sensor_recommendation` (`id_sensor_recommendation`, `id_sensor_var`, `recommendation`, `cost`, `resource`, `time_needed`) VALUES
	(1, 1, 'Periksa sistem saluran gas buang yang bocor & perbaiki jika perlu', 0, 0, 0),
	(2, 2, 'Periksa saluran gas buang atau udara masuk. Bersihkan jika perlu', 0, 0, 0),
	(3, 3, 'Periksa, ganti jika perlu, indikator (sensor)', 0, 0, 0);
/*!40000 ALTER TABLE `sensor_recommendation` ENABLE KEYS */;


-- Dumping structure for table monita3.sensor_var
CREATE TABLE IF NOT EXISTS `sensor_var` (
  `id_sensor_var` int(11) NOT NULL AUTO_INCREMENT,
  `id_sensor_group` int(11) NOT NULL DEFAULT '0',
  `state` enum('sensor_high','sensor_low','group_high','group_low') NOT NULL COMMENT 'which state this variable apply',
  `question` varchar(300) NOT NULL,
  `value` bit(1) NOT NULL COMMENT '0=false, 1=true',
  `last_check` datetime NOT NULL,
  PRIMARY KEY (`id_sensor_var`),
  KEY `FK_sv_sensor_group` (`id_sensor_group`),
  CONSTRAINT `FK_sv_sensor_group` FOREIGN KEY (`id_sensor_group`) REFERENCES `sensor_group` (`id_sensor_group`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='additional variable need to be checked before inference';

-- Dumping data for table monita3.sensor_var: ~13 rows (approximately)
DELETE FROM `sensor_var`;
/*!40000 ALTER TABLE `sensor_var` DISABLE KEYS */;
INSERT INTO `sensor_var` (`id_sensor_var`, `id_sensor_group`, `state`, `question`, `value`, `last_check`) VALUES
	(1, 1, 'group_high', 'Saluran gas buang bocor?', b'0', '0000-00-00 00:00:00'),
	(2, 1, 'group_high', 'Saluran gas buang atau udara masuk tersumbat?', b'0', '0000-00-00 00:00:00'),
	(3, 1, 'sensor_high', 'Setelan timing katup kurang sesuai?', b'0', '0000-00-00 00:00:00'),
	(6, 1, 'group_high', 'Beban yang dibangkitkan terlalu tinggi', b'0', '0000-00-00 00:00:00'),
	(7, 1, 'group_high', 'Volume udara masuk (tekanan) kurang', b'0', '0000-00-00 00:00:00'),
	(8, 1, 'group_high', 'Setelan timing katup kurang sesuai', b'0', '0000-00-00 00:00:00'),
	(9, 1, 'group_high', 'Setelan timing injeksi bahan bakar kurang sesuai', b'0', '0000-00-00 00:00:00'),
	(10, 1, 'group_high', 'Pengabutan injektor kurang baik', b'0', '0000-00-00 00:00:00'),
	(11, 1, 'group_high', 'Kebocoran saat langkah kompresi', b'0', '0000-00-00 00:00:00'),
	(12, 1, 'group_high', 'Kualitas bahan bakar', b'0', '0000-00-00 00:00:00'),
	(13, 1, 'sensor_high', 'Pengabutan injektor kurang baik', b'0', '0000-00-00 00:00:00'),
	(14, 1, 'sensor_high', 'Kebocoran saat langkah kompresi', b'0', '0000-00-00 00:00:00'),
	(15, 1, 'sensor_high', 'Kesalahan pembacaan pada indikator (sensor temperatur)', b'0', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sensor_var` ENABLE KEYS */;


-- Dumping structure for table monita3.users
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table monita3.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id_user`, `username`, `password`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
