(defclass Recommendation
	(is-a USER)
	(role concrete)
	(slot Value)
	(slot Next)
	(slot Cost)
	(slot Resource)
	(slot TimeNeeded)
)

;;;;;;;;;;;;;;;;;;;;;
;; HANDLER SECTION ;;
;;;;;;;;;;;;;;;;;;;;;

;; description: recursively add recommendation at the end of recommendations
(defmessage-handler Recommendation add-Recommendation (?recom)
	(if (eq (send ?self get-Next) nil) then
		(send ?self put-Next ?recom)
		else
		(send ?self:Next add-Recommendation ?recom)
	)
)

(defmessage-handler Recommendation print-Value ()
	(printout t ?self:Value crlf)
	(if (not (eq (send ?self get-Next) nil))
		then
			(send ?self:Next print-Value)
	)
)

;; check the recom is exist?
(defmessage-handler Recommendation need-isExistRecom (?recom)
	(if (eq (send ?self get-Value) ?recom) then
		(return ?self)
	else
		(if (not (eq (send ?self get-Next) nil)) then
			(send ?self:Next isExist-Recom)
		else
			(return false)
		)
	)
)