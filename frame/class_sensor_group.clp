(defclass SensorGroup
	(is-a USER)
	(role concrete)
	(slot Name)
	(slot HighLimit)
	(slot LowLimit)
	(slot FirstSensor)
	(slot IfSensorHigh)
	(slot IfSensorLow)
	(slot IfGroupHigh)
	(slot IfGroupLow)
)

;;;;;;;;;;;;;;;;;;;;;
;; HANDLER SECTION ;;
;;;;;;;;;;;;;;;;;;;;;

;; description: adding one sensor
(defmessage-handler SensorGroup add-Sensor (?sensor)
	(if (eq (send ?self get-FirstSensor) nil) then
		(send ?self put-FirstSensor ?sensor)
		else
		(send ?self:FirstSensor add-Sensor ?sensor)
	)
	(send ?sensor put-Parent (instance-name ?self))
)

;; description: count how much sensor in the sensor group
(defmessage-handler SensorGroup need-SensorNum ()
	(if (not (eq (send ?self get-FirstSensor) nil)) then
		(send ?self:FirstSensor need-SensorNum)
		else 0
	)
)

;; description: count total value sensor inside sensor group
(defmessage-handler SensorGroup need-TotalValue ()
	(if (not (eq (send ?self get-FirstSensor) nil)) then
		(send ?self:FirstSensor need-TotalValue)
		else 0
	)
)

;; description: count sensor group average value
(defmessage-handler SensorGroup need-Average ()
	(/ (send ?self need-TotalValue) (send ?self need-SensorNum))
)

;; description: add sensor_var to IfSensorHigh
(defmessage-handler SensorGroup add-IfSensorHigh (?sensor_var)
	(if (eq (send ?self get-IfSensorHigh) nil) then
		(send ?self put-IfSensorHigh ?sensor_var)
		else
		(send ?self:IfSensorHigh add-SensorVar ?sensor_var)
	)
)

;; description: add sensor_var to IfSensorLow
(defmessage-handler SensorGroup add-IfSensorLow (?sensor_var)
	(if (eq (send ?self get-IfSensorLow) nil) then
		(send ?self put-IfSensorLow ?sensor_var)
		else
		(send ?self:IfSensorLow add-SensorVar ?sensor_var)
	)
)

;; description: add sensor_var to IfGroupHigh
(defmessage-handler SensorGroup add-IfGroupHigh (?sensor_var)
	(if (eq (send ?self get-IfGroupHigh) nil) then
		(send ?self put-IfGroupHigh ?sensor_var)
		else
		(send ?self:IfGroupHigh add-SensorVar ?sensor_var)
	)
)

;; description: add sensor_var to IfGroupLow
(defmessage-handler SensorGroup add-IfGroupLow (?sensor_var)
	(if (eq (send ?self get-IfGroupLow) nil) then
		(send ?self put-IfGroupLow ?sensor_var)
		else
		(send ?self:IfGroupLow add-SensorVar ?sensor_var)
	)
)

;; description: need sensor_var IfSensorHigh
(defmessage-handler SensorGroup need-IfSensorHigh ()
	(if (not (eq (send ?self get-IfSensorHigh) nil)) then
		(send ?self:IfSensorHigh need-SensorVar)
	)
)

;; description: need sensor_var IfSensorLow
(defmessage-handler SensorGroup need-IfSensorLow ()
	(if (not (eq (send ?self get-IfSensorLow) nil)) then
		(send ?self:IfSensorLow need-SensorVar)
	)
)

;; description: need sensor_var IfGroupHigh
(defmessage-handler SensorGroup need-IfGroupHigh ()
	(if (not (eq (send ?self get-IfGroupHigh) nil)) then
		(send ?self:IfGroupHigh need-SensorVar)
	)
)

;; description: need sensor_var IfGroupLow
(defmessage-handler SensorGroup need-IfGroupLow ()
	(if (not (eq (send ?self get-IfGroupLow) nil)) then
		(send ?self:IfGroupLow need-SensorVar)
	)
)