(deffunction ask_Question (?question $?allowed-value)
	(printout t ?question)
	(bind ?answer (read))
	(if (lexemep ?answer)
		then (bind ?answer (lowcase ?answer))
	)
	(while (not (member ?answer ?allowed-value)) do
		(printout t ?question)
		(bind ?answer (read))
		(if (lexemep ?answer)
			then (bind ?answer (lowcase ?answer))
		)
	)
	?answer
)

(defclass SensorVar
	(is-a USER)
	(role concrete)
	(slot Question)
	(slot LastCheck)
	(slot SensorGroup)
	(slot FirstRecommendation)
	(slot Next)
	(slot flag (default 0))
)

;;;;;;;;;;;;;;;;;;;;;
;; HANDLER SECTION ;;
;;;;;;;;;;;;;;;;;;;;;


;; menambahkan daftar pertanyaan di akhir list
(defmessage-handler SensorVar add-SensorVar (?question)
	(if (eq (send ?self get-Next) nil)
		then (send ?self put-Next ?question)
		else (send ?self:Next add-SensorVar ?question)
	)
)

;; menambahkan rekomendasi
(defmessage-handler SensorVar add-SensorRec (?rec)
	(if (eq (send ?self get-FirstRecommendation) nil) then
		(send ?self put-FirstRecommendation ?rec)
	else
		(send ?self:FirstRecommendation add-SensorRec ?rec)
	)
)

;; return pertanyaan2
(defmessage-handler SensorVar need-SensorVar ()
	(send ?self put-flag 1)
	(if (not (eq (send ?self get-Next) nil))
		then (send ?self:Next need-SensorVar)
	)
)

;; kopi rec ke recommendation
(defmessage-handler SensorVar copy-Recommendation ()
	(if (not (eq (send ?self get-FirstRecommendation) nil)) then
		(send ?self:FirstRecommendation copy-SensorRec)
	)
)

;; send answer
(defmessage-handler SensorVar answer-yes ()
	(send ?self put-flag 0)
	(send ?self copy-Recommendation)
)