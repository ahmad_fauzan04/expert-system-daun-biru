(defclass Sensor
	(is-a USER)
	(role concrete)
	(slot Name)
	(slot Value)
	(slot Parent)
	(slot Next)
)

;;;;;;;;;;;;;;;;;;;;;
;; HANDLER SECTION ;;
;;;;;;;;;;;;;;;;;;;;;

;; description: recursively add sensor at the end of sensor group
(defmessage-handler Sensor add-Sensor (?sensor)
	(if (eq (send ?self get-Next) nil) then
		(send ?self put-Next ?sensor)
		else
		(send ?self:Next add-Sensor ?sensor)
	)
)

;; description: recursively count how much sensor in sensor group
(defmessage-handler Sensor need-SensorNum ()
	(if (not (eq (send ?self get-Next) nil)) then
		(+ 1 (send ?self:Next need-SensorNum))
		else 1
	)
)

;; description: recursively count total sensor value in sensor group
(defmessage-handler Sensor need-TotalValue ()
	(if (not (eq (send ?self get-Next) nil)) then
		(+ (send ?self get-Value) (send ?self:Next need-TotalValue))
		else (send ?self get-Value)
	)
)

;;;;;;;;;;;;;;;
;; INFERENCE ;;
;;;;;;;;;;;;;;;

;; description: inference start over here
(defmessage-handler Sensor set-Value (?value)
	(send ?self put-Value ?value)
	(if (not (eq (send ?self:Parent get-HighLimit) nil)) then
		(if (> (send ?self:Parent need-Average) (send ?self:Parent get-HighLimit)) then
			(send ?self:Parent need-IfGroupHigh)
		)
		(if (> ?value (send ?self:Parent get-HighLimit)) then
			(send ?self:Parent need-IfSensorHigh)
		)
	)
	(if (not (eq (send ?self:Parent get-LowLimit) nil)) then
		(if (< (send ?self:Parent need-Average) (send ?self:Parent get-LowLimit)) then
			(send ?self:Parent need-IfGroupLow)
		)
		(if (< ?value (send ?self:Parent get-LowLimit)) then
			(send ?self:Parent need-IfGroupLow)
		)
	)
	(send [Recom] print-Value)
)