;; Template

(make-instance [sensor_group.name] of SensorGroup
	(Name [sensor_group.name])
	(HighLimit [sensor_group.high_limit])
	(LowLimit [sensor_group.low_lomit])
)

;; foreach sensor in SensorGroup
(make-instance [sensor.name] of Sensor
	(Name [sensor.name])
	(Value 0)
)
(send [sensor_group.name] add-Sensor [sensor.nama])
;; end foreach

(make-instance Recom of Recommendations)

;; foreach sensor_var in sensor_group
(make-instance [sensor_var.name] of SensorVar
	(Question [sensor.question])
)

(send [sensor_group.name] add-IfGroupHigh [sensor_var.name])
(send [sensor_group.name] add-IfSensorHigh [sensor_var.name])
(send [sensor_group.name] add-IfGroupLow [sensor_var.name])
(send [sensor_group.name] add-IfSensorLow [sensor_var.name])

;; foreach recommendation in sersor_var

(make-instance [recomendation.name] of SensorRec
	(Value [recomendation.value])
)
(send [sensor_var.name] add-SensorRec [recommendation.name])

:: end foreach recommendation
;; end foreach sensor_var



