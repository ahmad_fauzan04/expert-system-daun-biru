(defclass Recommendations
	(is-a USER)
	(role concrete)
	(slot First)
)

;;;;;;;;;;;;;;;;;;;;;
;; HANDLER SECTION ;;
;;;;;;;;;;;;;;;;;;;;;

;; description: recursively add recommendation at the end of recommendations
(defmessage-handler Recommendations add-Recommendation (?value)
;;	(make-instance newRec of Recommendation
;;		(Value ?value)
;;	)
	(if (eq (send ?self get-First) nil) then
		(send ?self put-First ?value)
		else
		(send ?self:First add-Recommendation ?value)
	)
)

(defmessage-handler Recommendations print-Value ()
	(if (not (eq (send ?self get-First) nil))
		then
			(send ?self:First print-Value)
	)
)

(defmessage-handler Recommendations need-isExistRecom (?recom)
	(if (not (eq (send ?self get-First) nil)) then
		(send ?self:First need-isExistRecom ?recom)
	else
		(return false)
	)
)