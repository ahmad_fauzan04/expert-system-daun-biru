(defclass SensorRec
	(is-a USER)
	(role concrete)
	(slot Value)
	(slot Next)
	(slot Cost)
	(slot Resource)
	(slot TimeNeeded)
	(slot flag (default 0))
)

;;;;;;;;;;;;;;;;;;;;;
;; HANDLER SECTION ;;
;;;;;;;;;;;;;;;;;;;;;

;; menambahkan daftar rekomendasi di akhir list
(defmessage-handler SensorRec add-SensorRec (?recom)
	(if (eq (send ?self get-Next) nil)
		then (send ?self put-Next ?recom)
		else (send ?self:Next add-SensorRec ?recom)
	)
)

;; menyalin rekomendasi ke recom
;;(defmessage-handler SensorRec copy-SensorRec ()
;;	(if (not (eq (send [Recom] need-isExistRecom (send ?self get-Value)) false)) then
;;		(bind ?instance_name (sym-cat "rec" (instance-name ?self)))
;; (make-instance (send ?self get-Value) of Recommendation
;;	(Value (send ?self get-Value))
;;	(Cost (send ?self get-Cost))
;;	(Resource (send ?self get-Resource))
;;	(TimeNeeded (send ?self get-TimeNeeded))
;;)
;;		(send [Recom] add-Recommendation (send ?self get-Value))
;;	)
;;	(if (eq (send ?self get-Next) nil) then
;;		(send ?self:Next copy-SensorRec)
;;	)
;;)

(defmessage-handler SensorRec copy-SensorRec ()
	(send ?self put-flag 1)
	(printout t ?self:Value crlf)
	(if (not (eq (send ?self get-Next) nil)) then
		(send ?self:Next copy-SensorRec)
	)
)