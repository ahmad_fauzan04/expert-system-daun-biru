;; TEST Sensor & SensorGroup
(make-instance Exhaust-a of SensorGroup
	(Name Exhaust-a)
	(HighLimit 500)
	(LowLimit 60)
)

(make-instance Ex-a1 of Sensor
	(Name Exhaust-A1)
	(Value 500)
)

(make-instance Ex-a2 of Sensor
	(Name Exhaust-A2)
	(Value 500)
)

(make-instance Ex-a3 of Sensor
	(Name Exhaust-A3)
	(Value 600)
)

(make-instance Recom of Recommendations)

(send [Exhaust-a] add-Sensor [Ex-a1])
(send [Exhaust-a] add-Sensor [Ex-a2])
(send [Exhaust-a] add-Sensor [Ex-a3])


;; TEST SensorVar
(make-instance Ex-var-1 of SensorVar
	(Question "ada ini gak?")
)

(send [Exhaust-a] add-IfGroupHigh [Ex-var-1])

(make-instance Ex-var-2 of SensorVar
	(Question "ada itu gak?")
)

(send [Exhaust-a] add-IfGroupHigh [Ex-var-2])

(make-instance Ex-var-3 of SensorVar
	(Question "ada itu gak?")
)

(send [Exhaust-a] add-IfSensorHigh [Ex-var-3])

;; TEST SensorRec
(make-instance Ex-rec-1-1 of SensorRec
	(Value "rekomendasi 1 1")
)

(send [Ex-var-1] add-SensorRec [Ex-rec-1-1])

(make-instance Ex-rec-1-2 of SensorRec
	(Value "rekomendasi 1 2")
)

(send [Ex-var-1] add-SensorRec [Ex-rec-1-2])

(make-instance Ex-rec-3-1 of SensorRec
	(Value "rekomendasi 1 1")
)

(send [Ex-var-3] add-SensorRec [Ex-rec-3-1])

;; TEST CASE
(send [Ex-a1] set-Value 500)