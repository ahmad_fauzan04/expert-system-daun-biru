;; TEMPLATE
(deftemplate problem
	(slot text (type STRING))
)

;; RULES
(defrule check_bearing_A
	(bearing_temperature_condition high)
	=>
	(assert (problem (text "Temperature bantalan tinggi")))
)

;;(defrule check_bearing_B
;;	(active_power_condition high)
;;	=>
;;	(assert (problem (text "Beban yang dibangkitkan terlalu tinggi")))
;;)

;;(defrule check_bearing_C
;;	(lo_press_alarm low)
;;	=>
;;	(assert (problem (text "Tekanan minyak pelumas rendah")))
;;)