;; File : bahan_bakar_rules

;; TEMPLATE
(deftemplate output
	(slot recomm (type STRING))
)

(deftemplate ask
	(slot q (type STRING))
	(slot variable)
	(slot def)
)

;;RULES
(defrule rule_bahan_bakar_1
	(sfc ?sfc)
	(sfc_default ?sfc_default )
	(test (> ?sfc ?sfc_default))
	=>
	(assert (konsumsi lebih))
)

;; ask
(defrule konsumsi_lebih
	(konsumsi lebih)
	=>
	(assert (ask (q "kebocoran saluran bahan bakar?") (variable saluran_fuel_status) (def bocor)))
	(assert (ask (q "Beban yang dibangkitkan terlalu tinggi?") (variable beban_bangkit) (def high)))
	(assert (ask (q "Sinkronisasi beban antar unit tidak seimbang?") (variable sinkronasi_beban ) (def tidak_seimbang)))
	(assert (ask (q "kebocoran kompresi?") (variable compress_status) (def bocor)))
	(assert (ask (q "kesalahan pembacaan flowmeter?") (variable flowmeter_rightness) (def false)))
	(halt)
)

;; Konsumsi bahan bakar berlebih : kebocoran saluran bahan bakar
(defrule rule_bahan_bakar_2
	(konsumsi lebih)
	(value saluran_fuel_status bocor)
	=>
	(assert (output (recomm "Perbaiki kebocoran" )))
)

;; Konsumsi bahan bakar berlebih : beban yang dibangkitkan terlalu tinggi
(defrule rule_bahan_bakar_3
	(konsumsi lebih)
	(value beban_bangkit high)
	=>
	(assert (output (recomm "Turunkan beban yang dibangkitkan")))
)

;; Konsumsi bahan bakar berlebih : sinkronisasi beban antar unit tidak seimbang
(defrule rule_bahan_bakar_4
	(konsumsi lebih)
	(value sinkronasi_beban tidak_seimbang)
	=>
	(assert (output (recomm "Seimbangkan beban antar unit")))
)

;;Konsumsi bahan bakar berlebih : kebocoran kompresi
(defrule rule_bahan_bakar_5
	(konsumsi lebih)
	(value compress_status bocor)
	=>
	(assert (output (recomm "Periksa penyebab kebocoran kompresi, perbaiki jika perlu")))
)

;; Konsumsi bahan bakar berlebih : kesalahan pembacaan flowmeter
(defrule rule_bahan_bakar_6
	(konsumsi lebih)
	(value flowmeter_rightness false)
	=>
	(assert (output (recomm "Periksa, kalibrasi jika perlu, flowmeter")))
)