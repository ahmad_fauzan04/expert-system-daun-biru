;; File : bantalan_rules

;; TEMPLATE
(deftemplate output
	(slot recomm (type STRING))
)

(deftemplate ask
	(slot q (type STRING))
	(slot variable)
	(slot def)
)

;; RULES
(defrule bearing_temperature_condition_high
	(bearing_temperature_condition high)
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator tekanan?") (variable press_sensor_indicator) (def wrong)))
	(halt)
)

;; Temperatur Bantalan Tinggi:Beban yang dibangkitkan terlalu tinggi
(defrule rule_bantalan_1 
	(bearing_temperature_condition high)
	(active_power_condition high)
	=> 
	(assert (output (recomm "Turunkan beban yang dibangkitkan")))
)

;; Temperatur Bantalan Tinggi:Tekanan minyak pelumas rendah
(defrule rule_bantalan_2 
	(bearing_temperature_condition high)
	(lo_press_alarm low)
	=> 
	(assert (output (recomm "Lihat ‘Tekanan Minyak Pelumas Rendah’")))
)

;; Temperatur Bantalan Tinggi:Kesalahan pembacaan pada indikator (sensor tekanan)
(defrule rule_bantalan_3 
	(bearing_temperature_condition high)
	(value press_sensor_indicator wrong)
	=> 
	(assert (output (recomm "Periksa sensor tekanan dan ganti Jika perlu")))
)
