;; File : udara_pembakaran_rules

;; TEMPLATE
(deftemplate output
	(slot recomm (type STRING))
)

(deftemplate ask
	(slot q (type STRING))
	(slot variable)
	(slot def)
)

;; RULES
(defrule check_diff_A
	(TC_B_in ?B_in)
	(Bef_A_press ?A_press)
	(Threshold ?t)
	(test (> (- ?B_in ?A_press) ?t))
	=>
	(assert (delta_press_aftercooler_alarm high))
)

(defrule check_diff_B
	(TC_B_in ?B_in)
	(Bef_B_press ?B_press)
	(Threshold ?t)
	(test (> (- ?B_in ?B_press) ?t))
	=>
	(assert (delta_press_aftercooler_alarm high))
)

(defrule air_temp_alarm_high
	(air_temp_alarm high)
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator temperatur udara masuk?") (variable air_temp_indicator) (def wrong)))
	(assert (ask (q "Thermostat air pendingin kurang berfungsi?") (variable cooling_water_thermostat_condition) (def bad)))
	(assert (ask (q "Aftercooler buntu?") (variable aftercooler_status ) (def buntu)))
	(halt)
)

(defrule air_press_alarm_low
	(air_press_alarm low)
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator temperatur udara masuk?") (variable air_temp_indicator) (def wrong)))
	(assert (ask (q "kebocoran saluran udara masuk?") (variable saluran_air_input) (def bocor)))
	(assert (ask (q "filter udara buntu?") (variable air_filter_status ) (def buntu)))
	(assert (ask (q "bantalan TC dalam kondisi aus?") (variable bearing_status ) (def aus)))
	(assert (ask (q "Sudu-sudu (impeller) turbin tersangkut benda asing?") (variable impeller_status ) (def tersangkut)))
	(assert (ask (q "Sudu-sudu (impeller) turbin dalam keadaan kotor?") (variable impeller_status ) (def dirt)))
	(halt)
)

(defrule delta_press_aftercooler_alarm_high
	(delta_press_aftercooler_alarm high) 
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator tekanan udara masuk?") (variable press_bef_aftercooler_indicator) (def wrong)))
	(assert (ask (q "Aftercooler buntu?") (variable aftercooler_status ) (def buntu)))
	(halt)
)


;; Temperatur Udara masuk tinggi : Kesalahan pembacaan pada indikator
(defrule rule_udara_pembakaran_1 
	(air_temp_alarm high)
	(value air_temp_indicator wrong)
	=>
	(assert (output (recomm "Periksa dan Ganti Indikator temperatur jika perlu")))
)

;; Temperatur udara masuk tinggi : Temperatur air pendingin terlalu tinggi saat masuk aftercooler
(defrule rule_udara_pembakaran_2 
	(air_temp_alarm high)
	(cooling_water_temp_aftercooler high) ;; TODO
	=>
	(assert (output (recomm "Sesuaikan temperatur air masuk")))
)

;; Temperatur udara masuk tinggi : Thermostat air pendingin kurang berfungsi
(defrule rule_udara_pembakaran_3 
	(air_temp_alarm high)
	(value cooling_water_thermostat_condition bad)
	=>
	(assert (output (recomm "Periksa dan ganti Thermostat jika perlu" )))
)

;; Temperatur udara masuk tinggi : aftercooler buntu
(defrule rule_udara_pembakaran_4 
	(air_temp_alarm high)
	(value aftercooler_status buntu)
	=>
	(assert (output (recomm "Periksa dan ganti aftercooler jika perlu")))
)

;; Tekanan udara masuk rendah : kesalahan pembacaan indicator
(defrule rule_udara_pembakaran_5
	(air_press_alarm low)
	(value air_press_indicator wrong)
	=>
	(assert (output (recomm "Periksa dan ganti indikator jika perlu")))
)

;; Tekanan udara masuk rendah : kebocoran saluran udara masuk
(defrule rule_udara_pembakaran_6 
	(air_press_alarm low)
	(value saluran_air_input bocor)
	=>
	(assert (output (recomm "Perbaiki kebocoran saluran udara masuk" )))
)

;; Tekanan udara masuk rendah : filter udara buntu
(defrule rule_udara_pembakaran_7
	(air_press_alarm low)
	(value air_filter_status buntu)
	=>
	(assert (output (recomm "Bersihkan dan ganti filter udara jika perlu" )))
)

;; Tekanan udara masuk rendah : bantalan TC dalam kondisi aus
(defrule rule_udara_pembakaran_8 
	(air_press_alarm low)
	(value bearing_status aus)
	=>
	(assert (output (recomm "Overhaul turbocharger")))
)

;; Tekanan udara masuk rendah : sudu-sudu (impeller) turbin tersangkut benda asing
(defrule rule_udara_pembakaran_9 
	(air_press_alarm low)
	(value impeller_status tersangkut)
	=>
	(assert (output (recomm "Periksa, bersihkan jika perlu, sudu-sudu turbin dan Overhaul turbocharger")))
)

;; Tekanan udara masuk rendah : impeller turbin dalam keadaan kotor
(defrule rule_udara_pembakaran_10 
	(air_press_alarm low)
	(value impeller_status dirt)
	=>
	(assert (output (recomm "Bersihkan sudu-sudu turbin")))
)

;; Beda tekanan sebelum dan sesudah aftercooler tinggi : kesalahan pembacaan indicator
(defrule rule_udara_pembakaran_11 
	(delta_press_aftercooler_alarm high)
	(or (value press_bef_aftercooler_indicator wrong) (value press_aft_aftercooler_indicator wrong))
	=>
	(assert (output (recomm "Ganti Indikator Tekanan sebelum/sesudah aftercooler")))
)

;; Beda tekanan sebelum dan sesudah aftercooler tinggi : aftercooler buntu
(defrule rule_udara_pembakaran_12 
	(delta_press_aftercooler_alarm high) 
	(value aftercooler_status buntu)
	=>
	(assert (output (recomm "Bersihkan aftercooler")))
)
