;; TEMPLATES
(deftemplate output
	(slot recomm (type STRING))
)

(deftemplate ask
	(slot q (type STRING))
	(slot variable)
	(slot def)
)

;; RULES

;; ASK
; Ask for condition
(defrule active_power_condition_low
	(active_power_condition low)
	=>
	(assert (ask (q "Filter udara buntu?") (variable air_filter_condition) (def blocked)))
	(assert (ask (q "Kebocoran pada sistem saluran udara masuk atau gas buang?") (variable air_pipe_condition) (def cracked)))
	(assert (ask (q "Setelan timing injeksi bahan bakar kurang sesuai?") (variable injection_timing_condition) (def notnormal)))
	(halt)
)

;; Unit tidak dapat membangkitkan beban optimum:Filter udara buntu
(defrule rule_beban_1 
	(active_power_condition low)  
	(value air_filter_condition blocked)
	=> 
	(assert (output (recomm "Bersihkan filter udara dan ganti Jika perlu")))
)

;; Unit tidak dapat membangkitkan beban optimum: Tekanan udara masuk rendah
(defrule rule_beban_2 
	(active_power_condition low)  
	(air_press_alarm low)
	=> 
	(assert (output (recomm "Lihat 'Tekanan Udara Masuk Rendah'")))
)

;; Unit tidak dapat membangkitkan beban optimum: Kebocoran pada sistem saluran udara 
;; masuk atau gas buang
(defrule rule_beban_3 
	(active_power_condition low)
	(value air_pipe_condition cracked)
	=> 
	(assert (output (recomm "Periksa kebocoran pada saluran udara masuk atau gas buang danperbaiki Jika perlu")))
)

;; Unit tidak dapat membangkitkan beban optimum: Setelan timing injeksi bahan bakar kurang sesuai
(defrule rule_beban_4 
	(active_power_condition low)  
	(value injection_timing_condition notnormal)
	=> 
	(assert (output (recomm "Periksa setelan timing injeksi (terlalu mundur, terlalu lama)"))) 
	(assert (output (recomm "Periksa keausan pada mekanisme injeksi bahan bakar")))
)

