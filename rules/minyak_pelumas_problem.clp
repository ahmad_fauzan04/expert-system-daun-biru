;; TEMPLATE
(deftemplate problem
	(slot text (type STRING))
)

;; RULES
(defrule lo_press_alarm_low
	(lo_press_alarm low)
	=>
	(assert (problem (text "Tekanan minyak pelumas rendah")))
)

(defrule lo_press_alarm_high
	(lo_press_alarm high)
	=>
	(assert (problem (text "Tekanan minyak pelumas tinggi")))
)

(defrule lo_temp_alarm_high
	(lo_temp_alarm high)
	=>
	(assert (problem (text "Temperature minyak pelumas tinggi")))
)