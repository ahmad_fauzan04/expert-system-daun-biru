;; TEMPLATE
(deftemplate problem
	(slot text (type STRING))
)

;; RULES
(defrule rule_beban_low 
	(active_power_condition low)
	=> 
	(assert (problem (text "Beban yang dibangkitkan rendah")))
)

(defrule rule_beban_high 
	(active_power_condition high)
	=> 
	(assert (problem (text "Beban yang dibangkitkan tinggi")))
)