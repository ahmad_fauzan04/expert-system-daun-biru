;; TEMPLATE
(deftemplate problem
	(slot text (type STRING))
)

;; RULES
(defrule check_cylinder_problem
	(cyl ?A high)
	=>
	(assert (problem (text (str-cat "Masalah di cilinder " ?A))))
)

(defrule check_cyl_problem
	(cyl-val ?A ?num ?val)
	(avg ?A ?ave)
	(or (test (< ?val (- ?ave (* ?ave 0.1)))) (test (> ?val (+ ?ave (* ?ave 0.1)))))
	=>
	(assert (problem (text (str-cat "Masalah di cilinder " ?A ?num " .  Nilai melebihi rata-rata"))))
)