;; TEMPLATE
(deftemplate problem
	(slot text (type STRING))
)

;; RULES
(defrule temp_cooling_water_low
	(temp_cooling_water low)
	=>
	(assert (problem (text "Temperature air pendingin rendah")))
)

(defrule temp_cooling_water_high
	(temp_cooling_water high)
	=>
	(assert (problem (text "Temperature air pendingin tinggi")))
)

(defrule press_cooling_water_low
	(press_cooling_water low)
	=>
	(assert (problem (text "Tekanan air pendingin rendah")))
)