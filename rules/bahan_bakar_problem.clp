;; TEMPLATE
(deftemplate problem
	(slot text (type STRING))
)

;; RULES
(defrule rule_bahan_bakar_1
	(sfc ?sfc)
	(sfc_default ?sfc_default )
	(test (> ?sfc ?sfc_default))
	=>
	(assert (problem (text "Konsumsi bahan bakar berlebih")))
)
