;; TEMPLATE
(deftemplate problem
	(slot text (type STRING))
)

;; RULES
(defrule check_diff_A
	(TC_B_in ?B_in)
	(Bef_A_press ?A_press)
	(Threshold ?t)
	(test (> (- ?B_in ?A_press) ?t))
	=>
	(assert (problem (text "Perbedaan tekanan udara masuk dan keluar tinggi")))
)

(defrule check_diff_B
	(TC_B_in ?B_in)
	(Bef_B_press ?B_press)
	(Threshold ?t)
	(test (> (- ?B_in ?B_press) ?t))
	=>
	(assert (problem (text "Perbedaan tekanan udara masuk dan keluar tinggi")))
)

(defrule air_temp_alarm_high
	(air_temp_alarm high)
	=>
	(assert (problem (text "Temperatur Udara Masuk Tinggi")))
)

(defrule air_temp_alarm_low
	(air_temp_alarm low)
	=>
	(assert (problem (text "Temperatur Udara Masuk Rendah")))
)
