;; File : minyak_pelumas_rules

;; TEMPLATE
(deftemplate output
	(slot recomm (type STRING))
)

(deftemplate ask
	(slot q (type STRING))
	(slot variable)
	(slot def)
)

;; RULES

;; Ask for condition
(defrule lo_press_alarm_low
	(lo_press_alarm low)
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator (sensor temperatur)?") (variable lo_press_indicator) (def wrong)))
	(assert (ask (q "Saluran minyak pelumas ke gauge (sensor) buntu?") (variable lo_tube_gauge) (def blocked)))
	(assert (ask (q "Minyak pelumas tidak sesuai rekomendasi pabrikan?") (variable lo_status) (def not_recommended)))
	(assert (ask (q "Regulator tekanan minyak pelumas macet (posisi membuka)?") (variable lo_valve_condition) (def stuck)))
	(assert (ask (q "Filter (screen) minyak pelumas buntu?") (variable lo_screen_condition) (def blocked)))
	(assert (ask (q "Minyak pelumas berbusa?") (variable lo_status) (def foamed)))
	(assert (ask (q "Posisi kemiringan mesin melebihi batas maksimum?") (variable engine_slope_condition) (def high)))
	(assert (ask (q "Oil cooler kotor?") (variable lo_cooler_condition) (def dirty)))
	(assert (ask (q "Pompa minyak pelumas aus?") (variable lo_pump_condition) (def wornout)))
	(assert (ask (q "Keausan pada bearing?") (variable bearing_condition) (def wornout)))
	(assert (ask (q " Keausan pada rocker arm?") (variable rocker_arm_condition) (def wornout)))
	(assert (ask (q "Pipa saluran minyak pelumas retak (bocor)?") (variable lo_tube_condition) (def cracked)))
	(assert (ask (q "Kerusakan pada piston cooling jet?") (variable cooling_jet_piston_condition) (def broken)))
	(halt)
)

(defrule lo_press_alarm_high
	(lo_press_alarm high)
	=>
	(assert (ask (q "Regulator tekanan minyak pelumas macet (posisi menutup)?") (variable lo_valve_condition) (def stuck)))
)

(defrule lo_temp_alarm_high
	(lo_temp_alarm high)
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator (sensor temperatur)?") (variable lo_temp_indicator) (def wrong)))
	(assert (ask (q "Kondisi Cooler minyak pelumas kotor?") (variable lo_cooler_condition) (def dirty)))
	(assert (ask (q "Thermostat di oil cooler kurang berfungsi dengan baik?") (variable lo_cooler_thermostat_condition) (def notworking)))
	(assert (ask (q "Saluran air pendingin kotor dan tersumbat?") (variable radiator_condition) (def dirty)))
	(assert (ask (q "Saluran air pendingin kotor dan tersumbat?") (variable radiator_condition) (def dirty)))
	(halt)
)

;; Tekanan Minyak Pelumas Rendah: Kesalahan pembacaan pada indikator (sensor temperatur)
(defrule rule_minyak_pelumas_1 
	(lo_press_alarm low) 
	(value lo_press_indicator wrong)
	=> 
	(assert (output (recomm "Periksa sensor dan ganti Jika perlu")))
)

;; Tekanan Minyak Pelumas Rendah: Saluran minyak pelumas ke gauge (sensor) buntu
(defrule rule_minyak_pelumas_2 
	(lo_press_alarm low) 
	(value lo_tube_gauge blocked)
	=> 
	(assert (output (recomm "Bersihkan saluran minyak pelumas" )))
)

;; Tekanan Minyak Pelumas Rendah: Minyak pelumas tidak sesuai rekomendasi pabrikan
(defrule rule_minyak_pelumas_3 
	(lo_press_alarm low)
	(value lo_status not_recommended)
	=> 
	(assert (output (recomm "Ganti minyak pelumas dengan yang direkomendasikan pabrikan")))
)

;; Tekanan Minyak Pelumas Rendah: Regulator tekanan minyak pelumas macet (posisi membuka)
(defrule rule_minyak_pelumas_4 
	(lo_press_alarm low)
	(value lo_valve_condition stuck) 
	=> 
	(assert (output (recomm "Periksa kondisi dan bersihkan valve" )))
)

;; Tekanan Minyak Pelumas Rendah: Filter (screen) minyak pelumas buntu
(defrule rule_minyak_pelumas_5 
	(lo_press_alarm low) 
	(value lo_screen_condition blocked)
	=> 
	(assert (output (recomm "Periksa filter minyak pelumas, bersihkan, ganti jika perlu" )))
)

;; Tekanan Minyak Pelumas Rendah: Viskositas minyak pelumas terlalu rendah
(defrule rule_minyak_pelumas_6 
	(lo_press_alarm low)
	(lo_status viscocity_low)
	=> 
	(assert (output (recomm "Ganti minyak pelumas dengan yang direkomendasikan pabrikan")))
)

;; Tekanan Minyak Pelumas Rendah: Minyak pelumas berbusa
(defrule rule_minyak_pelumas_7 
	(lo_press_alarm low)
	(value lo_status foamed)
	=> 
	(assert (output (recomm "Periksa kandungan air dalam minyak pelumas, Ganti minyak pelumas dengan yang direkomendasikan pabrikan jika perlu")))
)

;; Tekanan Minyak Pelumas Rendah: Posisi kemiringan mesin melebihi batas maksimum
(defrule rule_minyak_pelumas_8 
	(lo_press_alarm low) 
	(value engine_slope_condition high)
	=> 
	(assert (output (recomm "Posisikan mesin sesuai dengan kemiringan yang direkomendasikan")))
)

;; Tekanan Minyak Pelumas Rendah: Oil cooler kotor
(defrule rule_minyak_pelumas_9 
	(lo_press_alarm low)
	(value lo_cooler_condition dirty)
	=> 
	(assert (output (recomm "Periksa oil cooler, bersihkan jika perlu")))
)

;; Tekanan Minyak Pelumas Rendah: Pompa minyak pelumas aus
(defrule rule_minyak_pelumas_10 
	(lo_press_alarm low)
	(value lo_pump_condition wornout)
	=> 
	(assert (output (recomm "Periksa oil cooler, bersihkan jika perlu")))
)

;; Tekanan Minyak Pelumas Rendah: Keausan pada bearing
(defrule rule_minyak_pelumas_11 
	(lo_press_alarm low)
	(value bearing_condition wornout)
	=> 
	(assert (output (recomm "Ganti bearing")))
)

;; Tekanan Minyak Pelumas Rendah: Keausan pada rocker arm
(defrule rule_minyak_pelumas_12 
	(lo_press_alarm low)
	(value rocker_arm_condition wornout)
	=> 
	(assert (output (recomm "Ganti rocker arm")))
)

;; Tekanan Minyak Pelumas Rendah: Pipa saluran minyak pelumas retak (bocor)
(defrule rule_minyak_pelumas_13 
	(lo_press_alarm low)
	(value lo_tube_condition cracked)
	=> 
	(assert (output (recomm "Perbaiki saluran minyak pelumas, ganti jika perlu")))
)

;; Tekanan Minyak Pelumas Rendah: Pipa saluran minyak pelumas retak (bocor)
(defrule rule_minyak_pelumas_14 
	(lo_press_alarm low)
	(value lo_tube_condition cracked)
	=> 
	(assert (output (recomm "Perbaiki saluran minyak pelumas, ganti jika perlu")))
)

;; Tekanan Minyak Pelumas Rendah: Kerusakan pada piston cooling jet
(defrule rule_minyak_pelumas_15 
	(lo_press_alarm low)
	(value cooling_jet_piston_condition broken)
	=> 
	(assert (output (recomm "Ganti piston cooling jet")))
)

;; Tekanan Minyak Pelumas Tinggi: Temperatur minyak pelumas terlalu rendah
(defrule rule_minyak_pelumas_16 
	(lo_press_alarm high)
	(lo_temp_alarm low)
	=> 
	(assert (output (recomm "Periksa kondisi thermostat")))
)

;; Tekanan Minyak Pelumas Tinggi: Temperatur minyak pelumas terlalu rendah
(defrule rule_minyak_pelumas_17 
	(lo_press_alarm high)
	(lo_status viscocity_high)
	=> 
	(assert (output (recomm "Ganti minyak pelumas dengan yang direkomendasikan")))
)

;; Tekanan Minyak Pelumas Tinggi: Regulator tekanan minyak pelumas macet (posisi menutup)
(defrule rule_minyak_pelumas_18 
	(lo_press_alarm high)
	(value lo_valve_condition stuck)
	=> 
	(assert (output (recomm "Periksa kondisi dan bersihkan valve" )))
)

;; Temperatur Minyak Pelumas Mesin Tinggi: Kesalahan pembacaan pada indikator (sensor temperatur)
(defrule rule_minyak_pelumas_19 
	(lo_temp_alarm high)
	(value lo_temp_indicator wrong)
	=> 
	(assert (output (recomm "Periksa sensor dan ganti Jika perlu" )))
)

;; Temperatur Minyak Pelumas Mesin Tinggi: Kesalahan pembacaan pada indikator (sensor temperatur)
(defrule rule_minyak_pelumas_20 
	(lo_temp_alarm high)
	(value lo_temp_indicator wrong)
	=> 
	(assert (output (recomm "Periksa sensor dan ganti Jika perlu" )))
)

;; Temperatur Minyak Pelumas Mesin Tinggi: Beban yang dibangkitkan terlalu tinggi 
(defrule rule_minyak_pelumas_21 
	(lo_temp_alarm high)
	(active_power_condition high)
	=> 
	(assert (output (recomm "Turunkan beban yang dibangkitkan" )))
	(assert (output (recomm "Periksa penyebab beban berlebihan" )))
)


;; Temperatur Minyak Pelumas Mesin Tinggi: Beban yang dibangkitkan terlalu tinggi 
(defrule rule_minyak_pelumas_22 
	(lo_temp_alarm high)
	(value lo_cooler_condition dirty)
	=> 
	(assert (output (recomm "Periksa oil cooler, bersihkan jika perlu")))
)

;; Temperatur Minyak Pelumas Mesin Tinggi: Thermostat di oil cooler kurang berfungsi dengan baik
(defrule rule_minyak_pelumas_23 
	(lo_temp_alarm high)
	(value lo_cooler_thermostat_condition notworking)
	=> 
	(assert (output (recomm "Periksa thermostat, ganti jika perlu")))
)

;; Temperatur Minyak Pelumas Mesin Tinggi: Saluran air pendingin kotor dan tersumbat
(defrule rule_minyak_pelumas_24 
	(lo_temp_alarm high)  
	(value radiator_condition dirty)
	=> 
	(assert (output (recomm "Bersihkan saluran air pendingin")))
)

;; Temperatur Minyak Pelumas Mesin Tinggi: Tekanan minyak pelumas rendah
(defrule rule_minyak_pelumas_25 
	(lo_temp_alarm high)
	(lo_press_alarm low) 
	=> 
	(assert (output (recomm "Lihat ‘Tekanan Minyak Pelumas Rendah’")))
)
