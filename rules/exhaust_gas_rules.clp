;; TEMPLATES
(deftemplate output
	(slot recomm (type STRING))
)

(deftemplate ask
	(slot q (type STRING))
	(slot variable)
	(slot def)
)

;; RULES
(defrule check_cylinder
	(not (exhaust_temp_cyl high))
	(cyl ?A high)
	=>
	(assert (exhaust_temp_cyl high))
)

(defrule check_cyl
	(not (exhaust_temp_cyl high))
	(cyl-val ?A ?num ?val)
	(avg ?A ?ave)
	(or (test (< ?val (- ?ave (* ?ave 0.1)))) (test (> ?val (+ ?ave (* ?ave 0.1)))))
	=>
	(assert (exhaust_temp_cyl high))
)

; Ask for condition
(defrule ask_exhaust_temp_all_high
	(exhaust_temp_all high)
	=>
	(assert (ask (q "Saluran Exhaust Bocor (y/t)") (variable saluran_exhaust) (def bocor)))
	(assert (ask (q "Saluran Exhaust Tersumbat (y/t)") (variable saluran_exhaust) (def tersumbat)))
	(assert (ask (q "Beban yang dibangkitkan tinggi (y/t)") (variable beban) (def high)))
	(assert (ask (q "Volume udara masuk rendah (y/t)") (variable volume_udara_masuk) (def low)))
	(assert (ask (q "Setelan timing katup kurang sesuai (y/t)") (variable setelan_timing_katup) (def kurang_sesuai)))
	(assert (ask (q "Setelan timing injeksi kurang sesuai (y/t)") (variable setelan_timing_injeksi) (def kurang_sesuai)))
	(assert (ask (q "Pengabutan injektor kurang baik (y/t)") (variable pengabutan_injektor) (def kurang_baik)))
	(assert (ask (q "Kebocoran saat langkah kompresi (y/t)") (variable kebocoran_saat_langkah_kompresi) (def true)))
	(assert (ask (q "Kualitas bahan bakar kurang baik (y/t)") (variable kualitas_bahan_bakar) (def kurang_baik)))
	(halt)
)

; Ask for condition
(defrule ask_exhaust_temp_cyl_high
	(exhaust_temp_cyl high)
	=>
	(assert (ask (q "Setelan timing katup kurang sesuai (y/t)") (variable setelan_timing_katup) (def kurang_sesuai)))
	(assert (ask (q "Pengabutan injektor kurang baik (y/t)") (variable pengabutan_injektor) (def kurang_baik)))
	(assert (ask (q "Kebocoran saat langkah kompresi (y/t)") (variable kebocoran_saat_langkah_kompresi) (def true)))
	(assert (ask (q "Kesalahan pembacaan pada indikator temperatur (y/t)") (variable kesalahan_pembacaan_pada_indikator_temperatur) (def true)))
	(halt)
)

; Convert ask to fact
; (defrule convert_ask
;	?f <- (ask ?a ?b y)
;	=>
;	(assert (value ?a ?b))
;	(retract ?f)
; )

; Saluran gas buang bocor: Periksa sistem saluran gas buang yang bocor & perbaiki jika perlu
(defrule saluran_gas_buang_bocor_dan_exhaust_bocor 
	(exhaust_temp_all high)
	(value saluran_exhaust bocor)
	=>
	(assert (output (recomm "Periksa saluran gas buang & bersihkan jika perlu")))
)

; Saluran gas buang atau udara masuk tersumbat: Periksa saluran gas buang atau udara masuk. 
(defrule saluran_gas_buang_bocor_dan_exhaust_tersumbat
	(exhaut_temp_all high)
	(value saluran_exhaust tersumbat)
	=>
	(assert (output (recomm "Periksa saluran gas buang & bersihkan jika perlu")))
)

; Beban yang dibangkitkan terlalu tinggi: Turunkan beban yang dibangkitkan
(defrule saluran_gas_buang_bocor_beban_tinggi
	(exhaust_temp_all high)
	(value beban high) 
	=>
	(assert (output (recomm "Turunkan beban yang dibangkitkan")))
)

; Volume udara masuk (tekanan) kurang: Periksa kondisi aftercooler jika terdapat sumbatan
(defrule saluran_gas_buang_bocor_tekanan_udara_masuk_rendah_saluran_tersumbat
	(exhaust_temp_all high)
	(value volume_udara_masuk low)
	(value saluran_exhaust tersumbat)
	=>
	(assert (output (recomm "Periksa kondisi aftercooler")))
)

(defrule saluran_gas_buang_bocor_volume_udara_masuk_low
	(exhaust_temp_all high)
	(value volume_udara_masuk low)
	=>
	(assert (output (recomm "Periksa kondisi turbocharger")))
)

; Setelan timing katup kurang sesuai: Periksa setelan timing katup; Periksa keausan pada mekanisme katup
(defrule setelan_timing_kurang_sesuai
	(exhaust_temp_all high)
	(value setelan_timing_katup kurang_sesuai) 
	=>
	(assert (output (recomm "Periksa setelan timing katup")))
	(assert (output (recomm "Periksa keausan pada mekanisme katup")))
)

; Setelan timing injeksi bahan bakar kurang sesuai: Periksa setelan timing injeksi (terlalu mundur, terlalu lama). Periksa keausan pada mekanisme injeksi bahan bakar
(defrule setelan_timing_injeksi_kurang_sesuai
	(exhaust_temp_all high)
	(value setelan_timing_injeksi kurang_sesuai) 
	=>
	(assert (output (recomm "Periksa setelan timing injeksi (terlalu mundur, terlalu lama)")))
	(assert (output (recomm "Periksa keausan pada injeksi bahan bakar")))
)

; Pengabutan injektor kurang baik: Periksa kondisi injektor (pegas & jarum); Periksa kualitas pengabutan injektor; Periksa tekanan injection pump
(defrule pengabutan_injektor_kurang_baik
	(exhaust_temp_all high)
	(value pengabutan_injektor kurang_baik) 
	=>
	(assert (output (recomm "Periksa kondisi injektor (pegas & jarum)")))
	(assert (output (recomm "Periksa kualitas pengabutan injektor")))
	(assert (output (recomm "Periksa tekanan injection pump")))
)

; Kebocoran saat langkah kompresi: Periksa tekanan carter;  Periksa kondisi ring piston. Periksa kondisi katup (valve)}
(defrule kebocoran_kompresi
	(exhaust_temp_all high)
	(value kebocoran_saat_langkah_kompresi true) 
	=>
	(assert (output (recomm "Periksa tekanan carter")))
	(assert (output (recomm "Periksa kondisi ring piston")))
	(assert (output (recomm "Periksa kondisi katup (valve)")))
)

; Kualitas bahan bakar : Lakukan fuel oil analysis untuk melihat kandungan mineral dalam bahan bakar 
(defrule kualitas_bahan_bakar_kurang
	(exhaust_temp_all high)
	(value kualitas_bahan_bakar kurang_baik) 
	=>
	(assert (output (recomm "Lakukan fuel oil analysis untuk melihat kandungan mineral dalam bahan bakar")))
)

; Setelan timing katup kurang sesuai: Periksa setelan timing katup; Periksa keausan pada mekanisme katup
(defrule setelan_timing_katup_cyl 
	(exhaust_temp_cyl high)
	(value setelan_timing_katup kurang_sesuai)
	=>
	(assert (output (recomm "Periksa setelan timing katup")))
	(assert (output (recomm "Periksa keausan pada mekanisme katup")))
)

; Pengabutan injektor kurang baik: Periksa kondisi injektor (pegas & jarum). Periksa kualitas pengabutan injektor. Periksa tekanan injection pump
(defrule pengabutan_injektor_cyl
	(exhaust_temp_cyl high)
	(value pengabutan_injektor kurang_baik) 
	=>
	(assert (output (recomm "Periksa kondisi injektor (pegas & jarum)")))
	(assert (output (recomm "Periksa kualitas pengabutan injektor")))
	(assert (output (recomm "Periksa tekanan injection pump")))
)

; Kebocoran saat langkah kompresi: Periksa tekanan carter.  Periksa kondisi ring piston
(defrule kebocoran_kompresi_cyl
	(exhaust_temp_cyl high)
	(value kebocoran_saat_langkah_kompresi true) 
	=>
	(assert (output (recomm "Periksa tekanan carter")))
	(assert (output (recomm "Periksa kondisi ring piston")))
	(assert (output (recomm "Periksa kondisi katup (valve)")))
)

; Kesalahan_pembacaan_pada_indikator_temperatur: Periksa, ganti jika perlu, indikator (sensor)
(defrule kesalahan_indikator_cyl
	(exhaust_temp_cyl high)
	(value kesalahan_pembacaan_pada_indikator_temperatur true) 
	=>
	(assert (output (recomm "Periksa, ganti jika perlu, indikator (sensor)")))
)

; Print recommendation
(defrule print_recomm
	(output (recomm ?recommendation))
	=>
	(printout t ?recommendation crlf)
)