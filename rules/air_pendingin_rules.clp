;; File : air_pendingin_rule

;; TEMPLATES
(deftemplate output
	(slot recomm (type STRING))
)

(deftemplate ask
	(slot q (type STRING))
	(slot variable)
	(slot def)
)

;; ASK 
(defrule temp_cooling_water_low
	(temp_cooling_water low)
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator suhu?") (variable indicator_rightness) (def false)))
	(assert (ask (q "Thermostat macet?") (variable thermostat_status) (def macet)))
	(halt)
)

(defrule temp_cooling_water_high
	(temp_cooling_water high)
	=>
	(assert (ask (q "Kesalahan pembacaan pada indikator suhu?") (variable indicator_rightness) (def false)))
	(assert (ask (q "Level air pendingin rendah?") (variable level_cooling_water) (def low)))
	(assert (ask (q "Thermostat macet?") (variable thermostat_status) (def macet)))
	(assert (ask (q "Saluran air pendingin kotor?") (variable saluran_cooler_water) (def kotor)))
	(assert (ask (q "Ada bubble?") (variable bubble_exist) (def true)))
	(assert (ask (q "Sirkulasi air pendingin buruk?") (variable circulation_cooling_water_status) (def bad)))
	(assert (ask (q "Pompa air pendingin bermasalah?") (variable pompa_cooling_water) (def bermasalah)))
	(assert (ask (q "Exhaust manifold retak?") (variable exhaust_manifold_status) (def retak)))
	(assert (ask (q "Gasket cylinder head bocor?") (variable gasket_cylinder_head_status) (def bocor)))
	(assert (ask (q "Setelan waktu injeksi kurang sesuai. Apakah terlalu lama ?") (variable injection_timing_setting) (def terlalu_lama)))
	(assert (ask (q "Setelan waktu injeksi kurang sesuai. Apakah terlalu mundur ?") (variable injection_timing_setting) (def terlalu_mundur)))
	(assert (ask (q "Cylinder head retak ?") (variable cylinder_head_status) (def retak)))
	(assert (ask (q "putaran kipas air pendingin rendah ?") (variable fan_cycle) (def low)))
	(assert (ask (q "Tutup radiator rusak ?") (variable tutup_radiator_status) (def broken)))
	(halt)
)

(defrule press_cooling_water_low
	(press_cooling_water low)
	=>
	(assert (ask (q "Pembacaan indikator tekanan salah ?") (variable press_indicator_rightness) (def false)))
	(assert (ask (q "Saluran air pendingin kotor ?") (variable saluran_cooling_water) (def kotor)))
	(assert (ask (q "Sirkulasi air pendingin kurang baik ?") (variable circulation_cooling_water_status) (def bad)))
	(assert (ask (q "Pompa air pendingin bermasalah ?") (variable pompa_cooling_water) (def bermasalah)))
	(halt)
)

;; RULES
;; Temperatur Air Pendingin Mesin Rendah: Periksa Kesalahan pembacaan pada indikator , ganti jika perlu
(defrule rule_air_pendingin_1 
	(temp_cooling_water low)
	(value indicator_rightness false)
	=>
	(assert(output (recomm "Periksa indikator, ganti jika perlu")))
)

;; Temperatur Air Pendingin Mesin Rendah: Periksa thermostat , ganti jika perlu
(defrule rule_air_pendingin_2
	(temp_cooling_water low)
	(value thermostat_status macet)
	=>
	(assert(output (recomm "ganti jika perlu")))
)

;; Temperatur Air Pendingin Mesin Tinggi : Periksa indikator, ganti jika perlu
(defrule rule_air_pendingin_3
	(temp_cooling_water high)
	(value indicator_rightness false)
	=>
	(assert (output (recomm "Periksa indikator air pendingin, dan ganti jika perlu")))
)

;; Temperatur air pendingin mesin tinggi : periksa level air pendingin dan tambahkan air pendingin
(defrule rule_air_pendingin_4
	(temp_cooling_water high)
	(value level_cooling_water low)
	=>
	(assert (output (recomm "Tambah air pendingin")))
)

;; Temperatur air pendingin mesin tinggi : periksa thermostat dan ganti jika perlu
(defrule rule_air_pendingin_5
	(temp_cooling_water high)
	(value thermostat_status macet)
	=>
	(assert (output (recomm "ganti termostat")))
)

;; Temperatur air pendingin mesin tinggi : periksa saluran air pendingin dan bersihkan
(defrule rule_air_pendingin_6
	(temp_cooling_water high)
	(value saluran_cooler_water kotor)
	=>
	(assert (output (recomm "Bersihkan sistem air pendingin")))
)

;; Temperatur air pendingin mesin tinggi : periksa akumulasi bubble dan buang udara yg ada dalam sistem
(defrule rule_air_pendingin_7
	(temp_cooling_water high)
	(value bubble_exist true)
	=>
	(assert(output (recomm "Buang udara yang ada dalam sistem air pendingin")))
)

;; Temperatur air pendingin tinggi : active_power_condition berlebihan, turunkan active_power_condition
(defrule rule_air_pendingin_8
	(temp_cooling_water high)
	(active_power_condition high)
	=>
	(assert (output (recomm "turunkan active_power_condition yang dibangkitkan")))
)

;; Temperatur air pendingin tinggi : active_power_condition berlebihan, periksa penyebab active_power_condition berlebihan 
(defrule rule_air_pendingin_9
	(temp_cooling_water high)
	(active_power_condition high)
	=>
	(assert (output (recomm "Periksa penyebab active_power_condition berlebihan"))) ;; TODO
)

;; Temperatur air pendingin tinggi : srkulasi air pendingin buruk
(defrule rule_air_pendingin_10
	(temp_cooling_water high)
	(value circulation_cooling_water_status bad)
	=>
	(assert (output (recomm "Periksa kondisi sistem air pendingin, Kerusakan seal pada pompa air pendingin, ganti jika perlu")))
)

;; Temperatur air pendingin tinggi : popa air pendingin bermasalah
(defrule rule_air_pendingin_11
	(temp_cooling_water high)
	(value pompa_cooling_water bermasalah)
	=>
	(assert (output (recomm "Periksa kondisi seal pompa air pendingin, periksa kondisi impeler pompa air pendingin, ganti jika perlu")))
)

;; Temperatur air pendingin tinggi : exhaust manifold retak
(defrule rule_air_pendingin_12
	(temp_cooling_water high)
	(value exhaust_manifold_status retak)
	=>
	(assert (output (recomm "Ganti exhaust manifold")))
)

;; Temperatur air pendingin tinggi : gasket cylinder head bocor
(defrule rule_air_pendingin_13
	(temp_cooling_water high)
	(value gasket_cylinder_head_status bocor)
	=>
	(assert (output (recomm "Ganti gasket")))
)

;; Temperatur air pendingin tinggi : setelan waktu injeksi kurang sesuai
(defrule rule_air_pendingin_14
	(temp_cooling_water high)
	(or (value injection_timing_setting terlalu_mundur) (value injection_timing_setting terlalu_lama))
	=> 
	(assert (output (recomm "periksa keausan pada mekanisme injeksi bahan bakar")))
)

;; Temperatur air pendingin tinggi : cylinder head retak
(defrule rule_air_pendingin_15
	(temp_cooling_water high)
	(value cylinder_head_status retak)
	=>
	(assert (output (recomm "ganti cylinder head")))
)

;; Temperatur air pendingin tinggi : putaran kipas air pendingin rendah
(defrule rule_air_pendingin_16
	(temp_cooling_water high)
	(value fan_cycle low)
	=>
	(assert (output (recomm "periksa kondisi belting dan keausan komponen")))
)

;; Temperatur air pendingin tinggi : tutup radiator rusak
(defrule rule_air_pendingin_17
	(temp_cooling_water high)
	(value tutup_radiator_status broken)
	=>
	(assert (output (recomm "Ganti komponen yang rusak")))
)

;; Tekanan air pendingin rendah : indikator salah
(defrule rule_air_pendingin_18
	(press_cooling_water low)
	(value press_indicator_rightness false)
	=>
	(assert(output (recomm "ganti indikator tekanan air pendingin tekanan")))
)

;; Tekanan air pendingin rendah : saluran air pendingin kotor
(defrule rule_air_pendingin_19
	(press_cooling_water low)
	(value saluran_cooling_water kotor)
	=>
	(assert (output (recomm "Bersihkan sistem air pendingin")))
)

;;Tekanan air pendingin rendah : sirkulasi air pendingin kurang baik
(defrule rule_air_pendingin_20
	(press_cooling_water low)
	(value circulation_cooling_water_status bad)
	=>
	(assert (output (recomm "Periksa kondisi sistem air pendingin. Kerusakan seal pada air pendingin")))
)

;; Tekanan air pendingin rendah : pompa air pendingin bermasalah
(defrule rule_air_pendingin_21
	(press_cooling_water low)
	(value pompa_cooling_water bermasalah)
	=>
	(assert (output (recomm "Periksa kondisi seal pompa air pendingin. Periksa kondisi impeler pompa air pendingin")))
)

; Print recommendation
(defrule print_recomm
	(output (recomm ?recommendation))
	=>
	(printout t ?recommendation crlf)
)